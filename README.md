# BPC-BDS Project 3 Hospital

## Co budeme potřebovat?
- pgAdmin/PostgreSQL
- Java OpenJDK 11
- Maven

## 1. Stažení gitu:
- `git clone https://gitlab.com/pisekm/bds-project-3.git`
- `cd bds-project-3`

## 2. Vytvoříme databázi:
- `psql -U postgres`
- postgres heslo
- `CREATE DATABASE BDS_Project;`
- `\q`
- `psql -U postgres BDS_Project < BDS_Project_db.sql`
- postgres heslo

## 3. Sprovoznění projektu:
- `mvn clean install`
- `java -jar target/bds-hospital-1.0.0.jar`

### Přihlašovací údaje:
- User Name: `user1` or `user2`
- Password: stejné jako User Name `user1`, `user2`

## 4. Automatický backup:
- backup se oběví každou půlnoc ve složce autobc:
- `scheduledTasksDB.bat`
#### 
- pokud chceme backup ihned:
- `backup.bat`






