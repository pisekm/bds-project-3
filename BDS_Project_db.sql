--
-- PostgreSQL database dump
--

-- Dumped from database version 14.0
-- Dumped by pg_dump version 14.0

-- Started on 2021-12-31 17:49:06

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 5 (class 2615 OID 2200)
-- Name: hospital; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA hospital;


ALTER SCHEMA hospital OWNER TO postgres;

--
-- TOC entry 3 (class 3079 OID 26155)
-- Name: pgcrypto; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA hospital;


--
-- TOC entry 3565 (class 0 OID 0)
-- Dependencies: 3
-- Name: EXTENSION pgcrypto; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';


--
-- TOC entry 2 (class 3079 OID 25967)
-- Name: unaccent; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS unaccent WITH SCHEMA hospital;


--
-- TOC entry 3566 (class 0 OID 0)
-- Dependencies: 2
-- Name: EXTENSION unaccent; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION unaccent IS 'text search dictionary that removes accents';


--
-- TOC entry 281 (class 1255 OID 25938)
-- Name: new_phone(character varying, integer); Type: PROCEDURE; Schema: hospital; Owner: postgres
--

CREATE PROCEDURE hospital.new_phone(IN new_number character varying, IN id integer)
    LANGUAGE plpgsql
    AS $$
BEGIN
	UPDATE contact c SET phone_number = new_number
	FROM patient p
	WHERE c.contact_id = p.contact_id AND p.patient_id = id;
END; $$;


ALTER PROCEDURE hospital.new_phone(IN new_number character varying, IN id integer) OWNER TO postgres;

--
-- TOC entry 280 (class 1255 OID 25936)
-- Name: where_is_patient(); Type: PROCEDURE; Schema: hospital; Owner: postgres
--

CREATE PROCEDURE hospital.where_is_patient()
    LANGUAGE plpgsql
    AS $$
begin
	select name, surname, department FROM patient p
	JOIN patient_in_department y ON y.patient_id = p.patient_id
	JOIN department de ON de.department_id = y.department_id 
	WHERE p.surname = surname1 and p.name = name1;
end; $$;


ALTER PROCEDURE hospital.where_is_patient() OWNER TO postgres;

--
-- TOC entry 279 (class 1255 OID 25928)
-- Name: where_is_patient(character varying, character varying); Type: PROCEDURE; Schema: hospital; Owner: postgres
--

CREATE PROCEDURE hospital.where_is_patient(IN name1 character varying, IN surname1 character varying)
    LANGUAGE plpgsql
    AS $$
begin
	select name, surname, department FROM patient p
	JOIN patient_in_department y ON y.patient_id = p.patient_id
	JOIN department de ON de.department_id = y.department_id 
	WHERE p.surname = surname1 and p.name = name1;
end $$;


ALTER PROCEDURE hospital.where_is_patient(IN name1 character varying, IN surname1 character varying) OWNER TO postgres;

--
-- TOC entry 266 (class 1255 OID 25940)
-- Name: young_patient(); Type: FUNCTION; Schema: hospital; Owner: postgres
--

CREATE FUNCTION hospital.young_patient() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
	IF NEW.date_of_birth > current_date - interval '18' year THEN
		 RAISE NOTICE 'The patient is under 18 years.';
	END IF;
	RETURN NEW;
END;$$;


ALTER FUNCTION hospital.young_patient() OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 235 (class 1259 OID 25256)
-- Name: address; Type: TABLE; Schema: hospital; Owner: postgres
--

CREATE TABLE hospital.address (
    address_id integer NOT NULL,
    city character varying(45) NOT NULL,
    street character varying(45) NOT NULL,
    house_number character varying(45) NOT NULL,
    zip_code integer NOT NULL
);


ALTER TABLE hospital.address OWNER TO postgres;

--
-- TOC entry 234 (class 1259 OID 25255)
-- Name: address_address_id_seq; Type: SEQUENCE; Schema: hospital; Owner: postgres
--

CREATE SEQUENCE hospital.address_address_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hospital.address_address_id_seq OWNER TO postgres;

--
-- TOC entry 3568 (class 0 OID 0)
-- Dependencies: 234
-- Name: address_address_id_seq; Type: SEQUENCE OWNED BY; Schema: hospital; Owner: postgres
--

ALTER SEQUENCE hospital.address_address_id_seq OWNED BY hospital.address.address_id;


--
-- TOC entry 212 (class 1259 OID 25134)
-- Name: contact; Type: TABLE; Schema: hospital; Owner: postgres
--

CREATE TABLE hospital.contact (
    contact_id integer NOT NULL,
    phone_number bytea NOT NULL,
    email bytea
);


ALTER TABLE hospital.contact OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 25133)
-- Name: contact_contact_id_seq; Type: SEQUENCE; Schema: hospital; Owner: postgres
--

CREATE SEQUENCE hospital.contact_contact_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hospital.contact_contact_id_seq OWNER TO postgres;

--
-- TOC entry 3571 (class 0 OID 0)
-- Dependencies: 211
-- Name: contact_contact_id_seq; Type: SEQUENCE OWNED BY; Schema: hospital; Owner: postgres
--

ALTER SEQUENCE hospital.contact_contact_id_seq OWNED BY hospital.contact.contact_id;


--
-- TOC entry 230 (class 1259 OID 25230)
-- Name: department; Type: TABLE; Schema: hospital; Owner: postgres
--

CREATE TABLE hospital.department (
    department_id integer NOT NULL,
    department character varying(45) NOT NULL,
    floor integer,
    room integer
);


ALTER TABLE hospital.department OWNER TO postgres;

--
-- TOC entry 229 (class 1259 OID 25229)
-- Name: department_department_id_seq; Type: SEQUENCE; Schema: hospital; Owner: postgres
--

CREATE SEQUENCE hospital.department_department_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hospital.department_department_id_seq OWNER TO postgres;

--
-- TOC entry 3574 (class 0 OID 0)
-- Dependencies: 229
-- Name: department_department_id_seq; Type: SEQUENCE OWNED BY; Schema: hospital; Owner: postgres
--

ALTER SEQUENCE hospital.department_department_id_seq OWNED BY hospital.department.department_id;


--
-- TOC entry 225 (class 1259 OID 25204)
-- Name: diagnose; Type: TABLE; Schema: hospital; Owner: postgres
--

CREATE TABLE hospital.diagnose (
    diagnose_id integer NOT NULL,
    diagnose character varying(45) NOT NULL
);


ALTER TABLE hospital.diagnose OWNER TO postgres;

--
-- TOC entry 224 (class 1259 OID 25203)
-- Name: diagnose_diagnose_id_seq; Type: SEQUENCE; Schema: hospital; Owner: postgres
--

CREATE SEQUENCE hospital.diagnose_diagnose_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hospital.diagnose_diagnose_id_seq OWNER TO postgres;

--
-- TOC entry 3577 (class 0 OID 0)
-- Dependencies: 224
-- Name: diagnose_diagnose_id_seq; Type: SEQUENCE OWNED BY; Schema: hospital; Owner: postgres
--

ALTER SEQUENCE hospital.diagnose_diagnose_id_seq OWNED BY hospital.diagnose.diagnose_id;


--
-- TOC entry 220 (class 1259 OID 25170)
-- Name: doctor; Type: TABLE; Schema: hospital; Owner: postgres
--

CREATE TABLE hospital.doctor (
    doctor_id integer NOT NULL,
    doctor_name character varying(45) NOT NULL,
    doctor_surname character varying(45) NOT NULL,
    contact_id integer NOT NULL
);


ALTER TABLE hospital.doctor OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 25169)
-- Name: doctor_contact_id_seq; Type: SEQUENCE; Schema: hospital; Owner: postgres
--

CREATE SEQUENCE hospital.doctor_contact_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hospital.doctor_contact_id_seq OWNER TO postgres;

--
-- TOC entry 3580 (class 0 OID 0)
-- Dependencies: 219
-- Name: doctor_contact_id_seq; Type: SEQUENCE OWNED BY; Schema: hospital; Owner: postgres
--

ALTER SEQUENCE hospital.doctor_contact_id_seq OWNED BY hospital.doctor.contact_id;


--
-- TOC entry 218 (class 1259 OID 25168)
-- Name: doctor_doctor_id_seq; Type: SEQUENCE; Schema: hospital; Owner: postgres
--

CREATE SEQUENCE hospital.doctor_doctor_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hospital.doctor_doctor_id_seq OWNER TO postgres;

--
-- TOC entry 3582 (class 0 OID 0)
-- Dependencies: 218
-- Name: doctor_doctor_id_seq; Type: SEQUENCE OWNED BY; Schema: hospital; Owner: postgres
--

ALTER SEQUENCE hospital.doctor_doctor_id_seq OWNED BY hospital.doctor.doctor_id;


--
-- TOC entry 251 (class 1259 OID 26151)
-- Name: dummy1; Type: TABLE; Schema: hospital; Owner: postgres
--

CREATE TABLE hospital.dummy1 (
    patient_id integer NOT NULL,
    name character varying(45),
    surname character varying(45),
    date_of_birth date NOT NULL,
    personal_no character varying(45),
    contact_id integer NOT NULL,
    insurance_company_id integer NOT NULL
);


ALTER TABLE hospital.dummy1 OWNER TO postgres;

--
-- TOC entry 250 (class 1259 OID 26148)
-- Name: dummy2; Type: TABLE; Schema: hospital; Owner: postgres
--

CREATE TABLE hospital.dummy2 (
    patient_id integer NOT NULL,
    name character varying(45),
    surname character varying(45),
    date_of_birth date NOT NULL,
    personal_no character varying(45),
    contact_id integer NOT NULL,
    insurance_company_id integer NOT NULL
);


ALTER TABLE hospital.dummy2 OWNER TO postgres;

--
-- TOC entry 241 (class 1259 OID 25283)
-- Name: hospital; Type: TABLE; Schema: hospital; Owner: postgres
--

CREATE TABLE hospital.hospital (
    hospital_id integer NOT NULL,
    hospital_name character varying(45) NOT NULL,
    address_id integer NOT NULL
);


ALTER TABLE hospital.hospital OWNER TO postgres;

--
-- TOC entry 240 (class 1259 OID 25282)
-- Name: hospital_address_id_seq; Type: SEQUENCE; Schema: hospital; Owner: postgres
--

CREATE SEQUENCE hospital.hospital_address_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hospital.hospital_address_id_seq OWNER TO postgres;

--
-- TOC entry 3587 (class 0 OID 0)
-- Dependencies: 240
-- Name: hospital_address_id_seq; Type: SEQUENCE OWNED BY; Schema: hospital; Owner: postgres
--

ALTER SEQUENCE hospital.hospital_address_id_seq OWNED BY hospital.hospital.address_id;


--
-- TOC entry 244 (class 1259 OID 25299)
-- Name: hospital_has_patient; Type: TABLE; Schema: hospital; Owner: postgres
--

CREATE TABLE hospital.hospital_has_patient (
    hospital_id integer NOT NULL,
    patient_id integer NOT NULL
);


ALTER TABLE hospital.hospital_has_patient OWNER TO postgres;

--
-- TOC entry 242 (class 1259 OID 25297)
-- Name: hospital_has_patient_hospital_id_seq; Type: SEQUENCE; Schema: hospital; Owner: postgres
--

CREATE SEQUENCE hospital.hospital_has_patient_hospital_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hospital.hospital_has_patient_hospital_id_seq OWNER TO postgres;

--
-- TOC entry 3590 (class 0 OID 0)
-- Dependencies: 242
-- Name: hospital_has_patient_hospital_id_seq; Type: SEQUENCE OWNED BY; Schema: hospital; Owner: postgres
--

ALTER SEQUENCE hospital.hospital_has_patient_hospital_id_seq OWNED BY hospital.hospital_has_patient.hospital_id;


--
-- TOC entry 243 (class 1259 OID 25298)
-- Name: hospital_has_patient_patient_id_seq; Type: SEQUENCE; Schema: hospital; Owner: postgres
--

CREATE SEQUENCE hospital.hospital_has_patient_patient_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hospital.hospital_has_patient_patient_id_seq OWNER TO postgres;

--
-- TOC entry 3592 (class 0 OID 0)
-- Dependencies: 243
-- Name: hospital_has_patient_patient_id_seq; Type: SEQUENCE OWNED BY; Schema: hospital; Owner: postgres
--

ALTER SEQUENCE hospital.hospital_has_patient_patient_id_seq OWNED BY hospital.hospital_has_patient.patient_id;


--
-- TOC entry 239 (class 1259 OID 25281)
-- Name: hospital_hospital_id_seq; Type: SEQUENCE; Schema: hospital; Owner: postgres
--

CREATE SEQUENCE hospital.hospital_hospital_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hospital.hospital_hospital_id_seq OWNER TO postgres;

--
-- TOC entry 3594 (class 0 OID 0)
-- Dependencies: 239
-- Name: hospital_hospital_id_seq; Type: SEQUENCE OWNED BY; Schema: hospital; Owner: postgres
--

ALTER SEQUENCE hospital.hospital_hospital_id_seq OWNED BY hospital.hospital.hospital_id;


--
-- TOC entry 214 (class 1259 OID 25141)
-- Name: insurance_company; Type: TABLE; Schema: hospital; Owner: postgres
--

CREATE TABLE hospital.insurance_company (
    insurance_company_id integer NOT NULL,
    insurance_company character varying(45) NOT NULL,
    insurance_company_code integer NOT NULL
);


ALTER TABLE hospital.insurance_company OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 25140)
-- Name: insurance_company_insurance_company_id_seq; Type: SEQUENCE; Schema: hospital; Owner: postgres
--

CREATE SEQUENCE hospital.insurance_company_insurance_company_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hospital.insurance_company_insurance_company_id_seq OWNER TO postgres;

--
-- TOC entry 3597 (class 0 OID 0)
-- Dependencies: 213
-- Name: insurance_company_insurance_company_id_seq; Type: SEQUENCE OWNED BY; Schema: hospital; Owner: postgres
--

ALTER SEQUENCE hospital.insurance_company_insurance_company_id_seq OWNED BY hospital.insurance_company.insurance_company_id;


--
-- TOC entry 217 (class 1259 OID 25149)
-- Name: patient; Type: TABLE; Schema: hospital; Owner: postgres
--

CREATE TABLE hospital.patient (
    patient_id integer NOT NULL,
    name character varying(45) NOT NULL,
    surname character varying(45) NOT NULL,
    date_of_birth date NOT NULL,
    personal_no character varying(45) NOT NULL,
    contact_id integer NOT NULL,
    insurance_company_id integer NOT NULL
);


ALTER TABLE hospital.patient OWNER TO postgres;

--
-- TOC entry 233 (class 1259 OID 25238)
-- Name: patient_in_department; Type: TABLE; Schema: hospital; Owner: postgres
--

CREATE TABLE hospital.patient_in_department (
    patient_id integer NOT NULL,
    department_id integer NOT NULL,
    date timestamp without time zone NOT NULL
);


ALTER TABLE hospital.patient_in_department OWNER TO postgres;

--
-- TOC entry 247 (class 1259 OID 25982)
-- Name: mat_number_of_pacient_in_interna; Type: MATERIALIZED VIEW; Schema: hospital; Owner: postgres
--

CREATE MATERIALIZED VIEW hospital.mat_number_of_pacient_in_interna AS
 SELECT h.hospital_name,
    count(p.patient_id) AS number_of_pacient_in_interna_in_each_hospital
   FROM ((((hospital.patient p
     JOIN hospital.hospital_has_patient q ON ((p.patient_id = q.patient_id)))
     JOIN hospital.hospital h ON ((h.hospital_id = q.hospital_id)))
     JOIN hospital.patient_in_department y ON ((y.patient_id = p.patient_id)))
     JOIN hospital.department de ON ((de.department_id = y.department_id)))
  WHERE ((de.department)::text = 'interna'::text)
  GROUP BY h.hospital_name
 HAVING (count(p.patient_id) > 1)
  WITH NO DATA;


ALTER TABLE hospital.mat_number_of_pacient_in_interna OWNER TO postgres;

--
-- TOC entry 223 (class 1259 OID 25186)
-- Name: patient_has_doctor; Type: TABLE; Schema: hospital; Owner: postgres
--

CREATE TABLE hospital.patient_has_doctor (
    patient_id integer NOT NULL,
    doctor_id integer NOT NULL
);


ALTER TABLE hospital.patient_has_doctor OWNER TO postgres;

--
-- TOC entry 245 (class 1259 OID 25942)
-- Name: pat_doc_depart; Type: VIEW; Schema: hospital; Owner: postgres
--

CREATE VIEW hospital.pat_doc_depart AS
 SELECT p.name,
    p.surname,
    d.doctor_name,
    d.doctor_surname,
    de.department,
    de.floor
   FROM ((((hospital.patient p
     JOIN hospital.patient_has_doctor q ON ((q.patient_id = p.patient_id)))
     JOIN hospital.doctor d ON ((d.doctor_id = q.doctor_id)))
     JOIN hospital.patient_in_department y ON ((y.patient_id = p.patient_id)))
     JOIN hospital.department de ON ((de.department_id = y.department_id)))
  ORDER BY p.surname;


ALTER TABLE hospital.pat_doc_depart OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 25148)
-- Name: patient_contact_id_seq; Type: SEQUENCE; Schema: hospital; Owner: postgres
--

CREATE SEQUENCE hospital.patient_contact_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hospital.patient_contact_id_seq OWNER TO postgres;

--
-- TOC entry 3604 (class 0 OID 0)
-- Dependencies: 216
-- Name: patient_contact_id_seq; Type: SEQUENCE OWNED BY; Schema: hospital; Owner: postgres
--

ALTER SEQUENCE hospital.patient_contact_id_seq OWNED BY hospital.patient.contact_id;


--
-- TOC entry 238 (class 1259 OID 25264)
-- Name: patient_has_address; Type: TABLE; Schema: hospital; Owner: postgres
--

CREATE TABLE hospital.patient_has_address (
    patient_id integer NOT NULL,
    address_id integer NOT NULL
);


ALTER TABLE hospital.patient_has_address OWNER TO postgres;

--
-- TOC entry 237 (class 1259 OID 25263)
-- Name: patient_has_address_address_id_seq; Type: SEQUENCE; Schema: hospital; Owner: postgres
--

CREATE SEQUENCE hospital.patient_has_address_address_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hospital.patient_has_address_address_id_seq OWNER TO postgres;

--
-- TOC entry 3607 (class 0 OID 0)
-- Dependencies: 237
-- Name: patient_has_address_address_id_seq; Type: SEQUENCE OWNED BY; Schema: hospital; Owner: postgres
--

ALTER SEQUENCE hospital.patient_has_address_address_id_seq OWNED BY hospital.patient_has_address.address_id;


--
-- TOC entry 236 (class 1259 OID 25262)
-- Name: patient_has_address_patient_id_seq; Type: SEQUENCE; Schema: hospital; Owner: postgres
--

CREATE SEQUENCE hospital.patient_has_address_patient_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hospital.patient_has_address_patient_id_seq OWNER TO postgres;

--
-- TOC entry 3609 (class 0 OID 0)
-- Dependencies: 236
-- Name: patient_has_address_patient_id_seq; Type: SEQUENCE OWNED BY; Schema: hospital; Owner: postgres
--

ALTER SEQUENCE hospital.patient_has_address_patient_id_seq OWNED BY hospital.patient_has_address.patient_id;


--
-- TOC entry 228 (class 1259 OID 25212)
-- Name: patient_has_diagnose; Type: TABLE; Schema: hospital; Owner: postgres
--

CREATE TABLE hospital.patient_has_diagnose (
    patient_id integer NOT NULL,
    diagnose_id integer NOT NULL
);


ALTER TABLE hospital.patient_has_diagnose OWNER TO postgres;

--
-- TOC entry 227 (class 1259 OID 25211)
-- Name: patient_has_diagnose_diagnose_id_seq; Type: SEQUENCE; Schema: hospital; Owner: postgres
--

CREATE SEQUENCE hospital.patient_has_diagnose_diagnose_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hospital.patient_has_diagnose_diagnose_id_seq OWNER TO postgres;

--
-- TOC entry 3612 (class 0 OID 0)
-- Dependencies: 227
-- Name: patient_has_diagnose_diagnose_id_seq; Type: SEQUENCE OWNED BY; Schema: hospital; Owner: postgres
--

ALTER SEQUENCE hospital.patient_has_diagnose_diagnose_id_seq OWNED BY hospital.patient_has_diagnose.diagnose_id;


--
-- TOC entry 226 (class 1259 OID 25210)
-- Name: patient_has_diagnose_patient_id_seq; Type: SEQUENCE; Schema: hospital; Owner: postgres
--

CREATE SEQUENCE hospital.patient_has_diagnose_patient_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hospital.patient_has_diagnose_patient_id_seq OWNER TO postgres;

--
-- TOC entry 3614 (class 0 OID 0)
-- Dependencies: 226
-- Name: patient_has_diagnose_patient_id_seq; Type: SEQUENCE OWNED BY; Schema: hospital; Owner: postgres
--

ALTER SEQUENCE hospital.patient_has_diagnose_patient_id_seq OWNED BY hospital.patient_has_diagnose.patient_id;


--
-- TOC entry 222 (class 1259 OID 25185)
-- Name: patient_has_doctor_doctor_id_seq; Type: SEQUENCE; Schema: hospital; Owner: postgres
--

CREATE SEQUENCE hospital.patient_has_doctor_doctor_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hospital.patient_has_doctor_doctor_id_seq OWNER TO postgres;

--
-- TOC entry 3616 (class 0 OID 0)
-- Dependencies: 222
-- Name: patient_has_doctor_doctor_id_seq; Type: SEQUENCE OWNED BY; Schema: hospital; Owner: postgres
--

ALTER SEQUENCE hospital.patient_has_doctor_doctor_id_seq OWNED BY hospital.patient_has_doctor.doctor_id;


--
-- TOC entry 221 (class 1259 OID 25184)
-- Name: patient_has_doctor_patient_id_seq; Type: SEQUENCE; Schema: hospital; Owner: postgres
--

CREATE SEQUENCE hospital.patient_has_doctor_patient_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hospital.patient_has_doctor_patient_id_seq OWNER TO postgres;

--
-- TOC entry 3618 (class 0 OID 0)
-- Dependencies: 221
-- Name: patient_has_doctor_patient_id_seq; Type: SEQUENCE OWNED BY; Schema: hospital; Owner: postgres
--

ALTER SEQUENCE hospital.patient_has_doctor_patient_id_seq OWNED BY hospital.patient_has_doctor.patient_id;


--
-- TOC entry 232 (class 1259 OID 25237)
-- Name: patient_in_department_department_id_seq; Type: SEQUENCE; Schema: hospital; Owner: postgres
--

CREATE SEQUENCE hospital.patient_in_department_department_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hospital.patient_in_department_department_id_seq OWNER TO postgres;

--
-- TOC entry 3620 (class 0 OID 0)
-- Dependencies: 232
-- Name: patient_in_department_department_id_seq; Type: SEQUENCE OWNED BY; Schema: hospital; Owner: postgres
--

ALTER SEQUENCE hospital.patient_in_department_department_id_seq OWNED BY hospital.patient_in_department.department_id;


--
-- TOC entry 231 (class 1259 OID 25236)
-- Name: patient_in_department_patient_id_seq; Type: SEQUENCE; Schema: hospital; Owner: postgres
--

CREATE SEQUENCE hospital.patient_in_department_patient_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hospital.patient_in_department_patient_id_seq OWNER TO postgres;

--
-- TOC entry 3622 (class 0 OID 0)
-- Dependencies: 231
-- Name: patient_in_department_patient_id_seq; Type: SEQUENCE OWNED BY; Schema: hospital; Owner: postgres
--

ALTER SEQUENCE hospital.patient_in_department_patient_id_seq OWNED BY hospital.patient_in_department.patient_id;


--
-- TOC entry 215 (class 1259 OID 25147)
-- Name: patient_patient_id_seq; Type: SEQUENCE; Schema: hospital; Owner: postgres
--

CREATE SEQUENCE hospital.patient_patient_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hospital.patient_patient_id_seq OWNER TO postgres;

--
-- TOC entry 3624 (class 0 OID 0)
-- Dependencies: 215
-- Name: patient_patient_id_seq; Type: SEQUENCE OWNED BY; Schema: hospital; Owner: postgres
--

ALTER SEQUENCE hospital.patient_patient_id_seq OWNED BY hospital.patient.patient_id;


--
-- TOC entry 246 (class 1259 OID 25958)
-- Name: teachers_view_on_user; Type: VIEW; Schema: hospital; Owner: postgres
--

CREATE VIEW hospital.teachers_view_on_user AS
 SELECT address.city,
    address.street,
    address.zip_code
   FROM hospital.address;


ALTER TABLE hospital.teachers_view_on_user OWNER TO postgres;

--
-- TOC entry 249 (class 1259 OID 26101)
-- Name: user; Type: TABLE; Schema: hospital; Owner: postgres
--

CREATE TABLE hospital."user" (
    user_id integer NOT NULL,
    username character varying(45) NOT NULL,
    password character varying(60) NOT NULL,
    name bytea,
    surname bytea
);


ALTER TABLE hospital."user" OWNER TO postgres;

--
-- TOC entry 248 (class 1259 OID 26100)
-- Name: user_user_id_seq; Type: SEQUENCE; Schema: hospital; Owner: postgres
--

CREATE SEQUENCE hospital.user_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hospital.user_user_id_seq OWNER TO postgres;

--
-- TOC entry 3628 (class 0 OID 0)
-- Dependencies: 248
-- Name: user_user_id_seq; Type: SEQUENCE OWNED BY; Schema: hospital; Owner: postgres
--

ALTER SEQUENCE hospital.user_user_id_seq OWNED BY hospital."user".user_id;


--
-- TOC entry 3319 (class 2604 OID 25259)
-- Name: address address_id; Type: DEFAULT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.address ALTER COLUMN address_id SET DEFAULT nextval('hospital.address_address_id_seq'::regclass);


--
-- TOC entry 3305 (class 2604 OID 25137)
-- Name: contact contact_id; Type: DEFAULT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.contact ALTER COLUMN contact_id SET DEFAULT nextval('hospital.contact_contact_id_seq'::regclass);


--
-- TOC entry 3316 (class 2604 OID 25233)
-- Name: department department_id; Type: DEFAULT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.department ALTER COLUMN department_id SET DEFAULT nextval('hospital.department_department_id_seq'::regclass);


--
-- TOC entry 3313 (class 2604 OID 25207)
-- Name: diagnose diagnose_id; Type: DEFAULT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.diagnose ALTER COLUMN diagnose_id SET DEFAULT nextval('hospital.diagnose_diagnose_id_seq'::regclass);


--
-- TOC entry 3309 (class 2604 OID 25173)
-- Name: doctor doctor_id; Type: DEFAULT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.doctor ALTER COLUMN doctor_id SET DEFAULT nextval('hospital.doctor_doctor_id_seq'::regclass);


--
-- TOC entry 3310 (class 2604 OID 25174)
-- Name: doctor contact_id; Type: DEFAULT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.doctor ALTER COLUMN contact_id SET DEFAULT nextval('hospital.doctor_contact_id_seq'::regclass);


--
-- TOC entry 3322 (class 2604 OID 25286)
-- Name: hospital hospital_id; Type: DEFAULT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.hospital ALTER COLUMN hospital_id SET DEFAULT nextval('hospital.hospital_hospital_id_seq'::regclass);


--
-- TOC entry 3323 (class 2604 OID 25287)
-- Name: hospital address_id; Type: DEFAULT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.hospital ALTER COLUMN address_id SET DEFAULT nextval('hospital.hospital_address_id_seq'::regclass);


--
-- TOC entry 3324 (class 2604 OID 25302)
-- Name: hospital_has_patient hospital_id; Type: DEFAULT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.hospital_has_patient ALTER COLUMN hospital_id SET DEFAULT nextval('hospital.hospital_has_patient_hospital_id_seq'::regclass);


--
-- TOC entry 3325 (class 2604 OID 25303)
-- Name: hospital_has_patient patient_id; Type: DEFAULT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.hospital_has_patient ALTER COLUMN patient_id SET DEFAULT nextval('hospital.hospital_has_patient_patient_id_seq'::regclass);


--
-- TOC entry 3306 (class 2604 OID 25144)
-- Name: insurance_company insurance_company_id; Type: DEFAULT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.insurance_company ALTER COLUMN insurance_company_id SET DEFAULT nextval('hospital.insurance_company_insurance_company_id_seq'::regclass);


--
-- TOC entry 3307 (class 2604 OID 25152)
-- Name: patient patient_id; Type: DEFAULT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.patient ALTER COLUMN patient_id SET DEFAULT nextval('hospital.patient_patient_id_seq'::regclass);


--
-- TOC entry 3308 (class 2604 OID 25153)
-- Name: patient contact_id; Type: DEFAULT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.patient ALTER COLUMN contact_id SET DEFAULT nextval('hospital.patient_contact_id_seq'::regclass);


--
-- TOC entry 3320 (class 2604 OID 25267)
-- Name: patient_has_address patient_id; Type: DEFAULT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.patient_has_address ALTER COLUMN patient_id SET DEFAULT nextval('hospital.patient_has_address_patient_id_seq'::regclass);


--
-- TOC entry 3321 (class 2604 OID 25268)
-- Name: patient_has_address address_id; Type: DEFAULT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.patient_has_address ALTER COLUMN address_id SET DEFAULT nextval('hospital.patient_has_address_address_id_seq'::regclass);


--
-- TOC entry 3314 (class 2604 OID 25215)
-- Name: patient_has_diagnose patient_id; Type: DEFAULT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.patient_has_diagnose ALTER COLUMN patient_id SET DEFAULT nextval('hospital.patient_has_diagnose_patient_id_seq'::regclass);


--
-- TOC entry 3315 (class 2604 OID 25216)
-- Name: patient_has_diagnose diagnose_id; Type: DEFAULT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.patient_has_diagnose ALTER COLUMN diagnose_id SET DEFAULT nextval('hospital.patient_has_diagnose_diagnose_id_seq'::regclass);


--
-- TOC entry 3311 (class 2604 OID 25189)
-- Name: patient_has_doctor patient_id; Type: DEFAULT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.patient_has_doctor ALTER COLUMN patient_id SET DEFAULT nextval('hospital.patient_has_doctor_patient_id_seq'::regclass);


--
-- TOC entry 3312 (class 2604 OID 25190)
-- Name: patient_has_doctor doctor_id; Type: DEFAULT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.patient_has_doctor ALTER COLUMN doctor_id SET DEFAULT nextval('hospital.patient_has_doctor_doctor_id_seq'::regclass);


--
-- TOC entry 3317 (class 2604 OID 25241)
-- Name: patient_in_department patient_id; Type: DEFAULT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.patient_in_department ALTER COLUMN patient_id SET DEFAULT nextval('hospital.patient_in_department_patient_id_seq'::regclass);


--
-- TOC entry 3318 (class 2604 OID 25242)
-- Name: patient_in_department department_id; Type: DEFAULT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.patient_in_department ALTER COLUMN department_id SET DEFAULT nextval('hospital.patient_in_department_department_id_seq'::regclass);


--
-- TOC entry 3326 (class 2604 OID 26104)
-- Name: user user_id; Type: DEFAULT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital."user" ALTER COLUMN user_id SET DEFAULT nextval('hospital.user_user_id_seq'::regclass);


--
-- TOC entry 3544 (class 0 OID 25256)
-- Dependencies: 235
-- Data for Name: address; Type: TABLE DATA; Schema: hospital; Owner: postgres
--

COPY hospital.address (address_id, city, street, house_number, zip_code) FROM stdin;
1	Plzeň	Stožická	13101	30100
2	Brno	Partyzánská	735	61200
4	Kladno	K Pazderně	29	27201
5	Jihlava	Palackého	782	58601
8	Praha	Jiřího z Poděbrad	88	10000
10	Praha	Pivovarská	1254	10400
11	Plzeň	Obránců míru	15103	30100
13	Most	Havlíčkova	921	43401
14	Liberec	Žižkova	1189	46014
16	České Budějovice	Stožická	1383	37004
17	Olomouc	Příční	8133	78365
18	Hradec Králové	Karafiátova	674	50003
19	České Budějovice	Mlýnská	117	37004
20	Olomouc	Československých legií	1880	78365
21	Opava	Družstevní	1477	74601
22	Brno	Petrželka	1084	60300
23	Brno	Jiráskova	681	61200
24	Zlín	Na Loučkách	1153	76001
25	Praha	Na Výsluní	1523	10000
26	Brno	Jiráskova	1995	60300
27	Jihlava	Jabloňová	1662	58601
28	Most	Tylova	1756	43401
29	Kladno	Jirsíkova	5136	27201
30	Hradec Králové	Pivovarská	1874	50003
31	Praha	Kovářská	387	10400
32	Brno	Na Loučkách 962		61200
33	Zlín	Žižkova	1899	76001
34	Jihlava	Sovova	1240	58601
35	České Budějovice	Polní	1433	37004
36	Plzeň	Zahradní	1113	31200
37	Opava	Nádražní	513	74601
38	Most	Tylova	1409	43401
39	Pardubice	Stožická	1027	53002
40	Praha	Fibichova	464	10100
41	Brno	Českého odboje	1503	61200
42	Olomouc	Nádražní	226	78365
43	Ostrava	Nepomuckého	203	70030
44	Praha	Na Výsluní	586	10400
45	Ostrava	Lidická	1254	70030
46	Most	Nepomuckého	205	43401
47	Jihlava	Nádraží	308	58601
48	Praha	Tyršova	1021	10000
49	Jihlava	Hamerská	1346	58601
50	Brno	Strojírenská	1397	60300
52	Brno	Zábrdovická	3/3	61500
53	České Budějovice	Boženy Němcové	585/54	37001
54	Hradec Králové	Sokolská	581	50005
55	Jihlava	Vrchlického	59	58601
56	Kladno	Vančurova	1548	27201
57	Liberec	Husova	10	46001
58	Most	Jana Evangelisty Purkyně	270	43464
59	Olomouc	I.P.Pavlova	185	77900
60	Ostrava	17. listopadu	1790/5	70800
61	Opava	Olomoucká	470/86	74601
62	Pardubice	Kyjevská	44	53003
63	Plzeň	Alej Svobody	80	32300
64	Praha	U Nemocnice	499/2	12808
65	Zlín	Havlíčkovo nábřeží	600	76275
51	Jevíčko	Tyllova	1692	27201
3	Pardubice	Poštovní	558	53003
9	Brno	Štefánikova	1232	60300
6	Ostrava	Opltova	84	70030
7	Zlín	Vostelčická	1136	76001
15	Liberec	Havlíčkova	1330	46014
12	Pardubice	Hrubínova	4104	53003
\.


--
-- TOC entry 3521 (class 0 OID 25134)
-- Dependencies: 212
-- Data for Name: contact; Type: TABLE DATA; Schema: hospital; Owner: postgres
--

COPY hospital.contact (contact_id, phone_number, email) FROM stdin;
2	\\xc30d04070302a7174286e37c00b27ad23a01cc331d3d23b79077c35f57ce2a2362f0e0e7eb0df72e454943623c9eac72256f836274fed47eab91c4c8d85d9eaff7ec031b7e4e30b9059ae5	\\xc30d040703028b16864a4c750c6767d24101e6680cce2a46a26b6f8bf4fab9e57c4397bad212b9a48c70d7ea1fbfb892471d137e55143c5abddd4ae02650f1babbb61cc27afe46779b78af067d9e3818a0e5
3	\\xc30d04070302c0fc1db47d9b76ed78d23a011b26a88cc7c3b191187c774b362372c440b22264c03aa9813dec43665e55b1b10cb6d26a08dd5e2b9137b584cab6822dcff1a6f205152b092f	\N
4	\\xc30d04070302da43f204537215516fd23a01113cd5a8aa035a2ea604c81137e17910d9f9a84d817342ecd65ad758e52485b36988c5351cd8d7045c04c70634d10645f125bd3a757241ab93	\N
5	\\xc30d0407030223551ce2c3140ec07cd23a01dafe6227abd98632235557e340a32a47d085c8e31ae2404ae7c9d726e1bec1d68d571a4d01845ce0c56899c2b49cb113d964d0cd8beb37634a	\\xc30d04070302dfa9e882ea9efa216dd24601016164837650d57a0bf70cdde74ff549bcd3910c57322cb0dd12673a7ad2bfdffe1d2266eb42e573d13d22a826ef8778bb368f7101c1bd48ea9b9f7ced4e7751a19e49628f
6	\\xc30d04070302910f9a30e8f2760669d23a014830876e2445f458c23d92e83f04def064cb86997db7047e132ba6cfd23d650645e45d9958252bc2fc2cd79afbb17d24dfa0b2eee2becea30d	\N
7	\\xc30d04070302c6ef6cbaa91fa69f70d23a01b0d8fe8e63acf71d38339aa26b4da748be4210e0b649ecfa626c34a137d26cce806cb7674ae990d84785fded2eeb19713118539e4422728121	\\xc30d04070302ba022c4e129ed2316dd24301b319bd4fd481fc98f84ce859521467a32c3e2b669ab3ea9a1aa94339e26b3f1843b65d14608b85b0d8d7cee10a13f22bdc561cbdf98fbfa8faefdd671cd617809283
8	\\xc30d040703029b37fc69180b110961d23a01e94bdbc6ab5174fc6fc2f0bced9edb669e1462dc0a6d96f631ac2718bebd45a81c055caf104a4b3fb160405414edda3a7d5e5004896b53613e	\\xc30d04070302b23521019290034b63d245017824774268ab4b311b8aa8f591535ddbcd1434bc0330756ac705e87837a8d95ebfc64360fa5a7e3000bf981b10a0f82d62364e3bf7354f15459eff651981e65fe1f9f70c
9	\\xc30d040703025255465381747eeb74d23a0184c7682100f1e3fbac387455faf27e55a49c40f259d395884fbcf9d8bc092317a719d441d2d6512550d14bd49e3754fc88be03262bfff17086	\N
10	\\xc30d04070302eff805f394669ede6ed23a0128033988fd81986602d74862bedff89301c6c400e70e809efbd45205ea45e7d67490547f676e386b56c5b1a1e1581b36da10e9ba80d64b89fa	\\xc30d040703024d1a0d4c7be2ea9679d2470175c2dc80f2497726aa81cd5052286fb1fcda63366bf3e7ae5317f350fc27d532c8b3667b71322dab28ac9e69368da3604230f4029479e1f7e37a684a8f986342446a5cc4be72
11	\\xc30d04070302f9b30bbf5a9ae6857ed23a01a08a9312322dce0048287f82c3ec9edc3b2c7fc9d7bdc7d80274faaae6832bb352921c1c5e3c434cdfa3616c0d28db3f54058cd60edd1a0e0a	\N
12	\\xc30d0407030279dbbaf97165023468d23a01bf4a7d2bbc318b87e48c47775dcde7d7d371a8fb24e57f756b99a0b1a691435594f55961793efd35afdac164d1fa8d5a7d41eb756f2069b44d	\\xc30d040703028580a8b3d786677169d24401fe6d8c2edc6702c3a792c50d1f155a979ca06f0b75d6dfd9d8dc77a7f5966f151fe1eb3abe46b7af07ab9f4e407311a0512c407d9928df8e33c4ab9d2a69a8ff80a21b
13	\\xc30d04070302bf7936117cb37fe977d23a01eb2a4277113665179a94421578df7fa546b39487c9be02ffac34a2ee65d7efeeb61f6af02d04ad4926ae38ef660f496ed7ac3c7b20b7bb9ac9	\\xc30d04070302512965f71144d88d7ed24501e30bf29e9f25be7b68a388c89e8c2f99313333acd1bc21ba8a1959c70b31548d03af5236ee4a138d8a9e8013b5e57cbc58e047896bd00ca61dbacf18783c0216cc5b38d4
14	\\xc30d0407030230d26130000b5f5f64d23a01ed666f0400fc56656e1518ac1c0dbb421d5f990294a1cb0b2e77d3aad4169762428702208fa8b3fb99c0a251ca9d5eeddc1775c9d0aa683757	\N
15	\\xc30d04070302079cf13d9ef7a6a56dd23a012644b771c5e6f065c664cb5e9aaae4c7037605ed400adc59d25b5daa956a4ec5d6a83ed04a44dfb2207d873544f0ce07d563cf39a1dc79dcc3	\N
16	\\xc30d04070302508f426616dab53c64d23a01ce1666819a23a581e2ff30461d15e2de09c46c9486e7d92e2755f5e1002b065a481d1097ac6f16680482f3b1a883c633672eb02e4e7b1fe0ee	\N
17	\\xc30d04070302c06c8d1c8329a9f67dd23a01d9faf177414ad372fbc40ab186201f1d8e22a60733adff6e444e65092b580024b3d8e01ceafc83a0d1099d576f63a3f84dca8a2e678841dcc9	\N
18	\\xc30d040703021a0fd74c687a9f3f79d23a01c87590000253b3541c2c83fae3bc7789ebfc85570d68c6473f8e2dd5994c66ddead9ab6c7ee78e45d17c776fca85cb4a834d9dd33c9203052b	\N
19	\\xc30d04070302c174fb894fff770e77d23a014c4fe3149e917812de67b2d64f7fa8e42e8fe95740c2cbde8caf772ed352410816776ead9783b4e7df9bee50ec779fd1cf073853f344f02b70	\N
20	\\xc30d04070302f82022888b380df879d23a0154d409ad89c34818d289a40dabacdbd2a91c0856093dc1d843bad73b0ca6b714ea724bf957847bda755a00459e279387a6460a7fc32d377e8d	\\xc30d04070302abd982c317cada8965d241018b4223c1b4c3dc3c5ff05cb414f68d40a7f9cc2a43e8ce920fb6182817ae02c7bd54e54fe628f9fafc61bdf42a1a09c6211b815a9d5807f2e60d48278efe9757
21	\\xc30d0407030244b342443f8766dd6dd23a01c2ab76850b16879c312db33b718d87f77d6ed50f17d2064f699ff3378ce66051f9cb2f7639f508997b1697381564fcbb728e87216d1d9fa72e	\N
22	\\xc30d04070302c9b1eaadb82b1be964d23a01b858e85df7b31f4f298786f902486f43209f102c1f1bbf8ef8b79d327b790e82a034dc5b0a396f46ae160cdccb7563b8eea264963123f71228	\N
23	\\xc30d0407030291437afb29be5a9477d23a014617304a44ef3dbfa61b638ec22c74117cfa7002192ff53e667c5a51c4966df576d1ba3a885b04ce32a8f65ae33533aabb6a62918f4b2f8ed0	\\xc30d04070302a4e2ecfeb6a74c877bd24101a8e8300b76aa8425caa41c143c02a49aba06f5ef74cd7d1e000d1062554b2e45fc40442f266838ee3ec556714c91a8c97e563e7f367bb269b1943664a4b1996c
24	\\xc30d04070302080de881b03524e077d23a016cafabea732e94b2bb1e20a89065f8fac27d0a758c52709cde4421b6841825d193251056c5329d036941bc16312d5d1ba03eb7ddb655d4d6a8	\N
25	\\xc30d04070302f8aae13547e880ee69d23a01e18b59f014b044cad8d864fbad680be6f402bdac57218e639cc5094a7f22d33cac1a2fcf785d2af94ff2af6000a2ee19f83df1c91e652733ac	\N
26	\\xc30d040703022bc3f489ef98644a6fd23a0188217813d5781253561ce3154539d1e64e15af44b6ded3f8c18fd693f65b75f102263bb12644a7d7ee1f28458da8f3e3d736c37199913ddf5d	\\xc30d040703026b841f167ad9e0f974d2410174ca3d367bf3e80513f19306b0451568ab36c260bfebc42fb20cc64b7b718041408f81c5799d3b5e1cd0fc838a9585d6567f712b2677666e48bdb4baa087912d
27	\\xc30d0407030264b63b0617031e8476d23a010cb3de805588c511d312c2bd0da01c9513e301d8cc241428776a9c6dc0ffa6bf9d251225e9ec01c24abae80e785844016df65bc7907dd2ff50	\\xc30d04070302b4f06f23877abd9272d24001c5f727da184809746f68c335363f519ff5205af6c85c0c248fc6bd7de2f72992e93e94d471baea8eab6957aa9aa6f59149c9c1fd769bdc9c9efb22dd910e5f
28	\\xc30d04070302cb8b4822d8ab3ce270d23a01760b932742e2c270bb1ebd1186ebb89a14e99bc79bba436f63df76fe7145ad5a07840a5936edae04e94207f3d99090d9a3fcf20592470561a4	\N
29	\\xc30d04070302eb5539a49eaa6dbd69d23a015fc2173f29792c295193e46edbfcb3f137f463adba2973a0b1ff2ae3e98154effd1e17c2cc3eb1b75c584757b577d357690599013f43e95a3c	\N
31	\\xc30d040703026a2a3a7cfb1292d667d23a01bf30fe9da312c9cea48fed67d45250b2a0fdd5ceba1cef1a2c5e9ac84b9cbf4320a25e41541128fc9c291a976bcf15d4b40a2533a919f399d4	\N
32	\\xc30d04070302fd3deddc23117d1662d23a01410bc38c33017296e3b9137ca6985dde85d939c6865981adb1eb8a8bdb1481b53a2cd96e2a4efdb72e52e204ade28a2758a994cda4c0e0aba9	\\xc30d040703029e726fde6656550b69d2440106d6cdde87cb197c0fd60d2107ca1dbf6680dea7ee4706677063a4f6cf7636a90949c69102bbf43a09445ae578d5b468bcddd4284da388b390607d71e628fa0d5603c6
33	\\xc30d040703025b2c3a8423df08787cd23a014ff222f167cce3c52e68712aa2756558be978c840dae213e8949e1aeb9c96a5ffe3026b04a202b3d84965b5ab7cf2e5fb1376cf9c5c32b3394	\\xc30d040703024aab9583b991c7c574d24501ab8e742d76542337358d755ed52ac78426d6dd1a4ec60c2ca8ce25b5555e23b7a94e130278f80b8a997a4c892114e7ccf314afd7bd22b3dcf08cbb1870de3858166bbe8a
34	\\xc30d040703027e654045403c6ec27bd23a011407e72b160d3f916206d5748210ac1fa0ad159cb962b6d8dd5794c0dfd21cff2e4e7d4fcce25a1053b5774d5f3e551c00bed8a7d1d3f96f2a	\N
35	\\xc30d04070302d339a4beccfc0fc162d23a01b4718bfc7eddd5a3d8db5f0508c83b0c2ab34f2200835ce1dd50551852b34052cfcd7cc6031daba487cd38d96eaed458c8ca4867745fa045f6	\N
36	\\xc30d04070302376bb792c4db9e2c76d23a014b843c0a873f4c7e2584f791c0dbfeaa110a6f6aee56bdce94b5e704fcea2591ef399de0570ddac66dfcb35d00821cb0610846440b4a5b82cc	\\xc30d04070302b4d51ffff8068b977cd244017790486adae92584ad822ea04911ae5fe1e2c9e7b4ec0db0e8b97b8dd715cb8cd5137129c9f886dc3860a16e51386c8d4bd7a86339cc30199d33bcdf3bc573bb58971c
37	\\xc30d04070302b4a0ae825f15014874d23a0158736fb3d0b7d53e109723f49f5ea1ace4f2e7bba9981c5114b4dcb941cbcbc4e0165f18c24fedd63286959932662006b0e16169d3d6ccbf03	\N
38	\\xc30d0407030270f31f814bd857376bd23a01abaaeabe2f440ce22195ba976b247e3e78747c3235598f35680b2cb0b9b4a1235ac1256e6a99bfbd6c3436b4220be82d457a16eea8ce6adda3	\N
39	\\xc30d04070302fb0a4d345abf11a367d23a01c84ff85836a69eea5786f8cc06676942604b47616435fc34595fbad9e03192a69c05e73ecb672051c5515fbf07f472e9335d7fd9717087be4e	\\xc30d0407030231b0ac7b063b086075d246017b9ca2595f067bc8f63e8bded64b02b449b3293d7a6acfa4cc2846139f6e370ec7db44530d150bcee587166d778a73f1fe6bf616270a7f86b41cfa1086a94a46281cc7c638
40	\\xc30d0407030282bb6cc9ec4847737bd23a017e513900e48e5dac21ea0a59452c5790d27494998a45d90ac2b2986fce783169314d06cd2160f26c0821303ef2db9dbfa54496f73e475321e8	\N
41	\\xc30d0407030210c169e898633f9b67d23a01518de7bc6ddbb739b94d600df090c36177c17c553e7f96f6eca44e204ed43dd1e597995d3694aef4313ae65855df682d24c1437bb1bb0f1b86	\\xc30d04070302357628b62e5ca38163d242011736d442e26560d6b04099deb9a1f752fbc9890c4e02bf5c1b37e8fcdb5467b09c461cd24dec8afbbfd87829505f746851defcd5f627d2fcc90302d70df9c8bcd3
42	\\xc30d040703027b7404f25726257162d23a0107629cc9ee6bc98b1ff016b3e538b372dffa4d88e54d816c5355200bfc4c359c26f574a68664464576246ecbc3f39bd6e47add518821f74ffb	\N
43	\\xc30d04070302a45f3e89a529d7e370d23a01387043d08af7dc345d079d653932a5e008d45c39f9cf14fc1b5982ae0f1fb138ff69f1d7cac4fe9f08c876217953b393a8b023cc846448f1d8	\N
44	\\xc30d04070302dd675fead51bb8447dd23a01b33a3ac02a76d367ad08f9a35c31ef680ae3db5edb7ddafbf5810f7249129bcc78ecd8aa09b864dc3be7253dbb341850f4b6cc94cd7c27fa39	\N
45	\\xc30d04070302d54c96e1dc4d47e775d23a016e6b6cad47b52cbed0a428e8b712dd37293420466852e949ac5f147823752fe29b559ef42365fd41745da41e3e4e5d5a91e08e7e4a2b6b3452	\N
46	\\xc30d040703029ea9530661ae00c976d23a01b209cf8ab494a5fa7255c73d5dd35e34ac074f11840d551d129e1cc7e7c04aa21974d3cb1f82414b05716a30da06be969a213b16b5a3a01305	\N
47	\\xc30d0407030273551a28e30e7b8b6ed23a013cf88a298922cdad83e42ad44c14f3fb0803e47a090929664ea405dec9f67d6cd1aed5913de28020dd2a78d8d501b4d3f1bec9e6a16d406151	\\xc30d040703025bfb3ef38abfb47f7bd243013d0e94787a80c300597faeda6ff5b632a16766533e10ccb51373170bf94249da3dbf121bfff3edf3f5fb89101df44dfbc7ecee65e3af2540002c2194941fb98ac3ab
48	\\xc30d0407030257adec2865f1e6807fd23a0181696b3ef73f7f76267ce236f3be85fbaeb2d42ea8fa28bfa31dfb542952b977efd86b1a9ce35070a01f21070dc4971182c635f6995e0edffc	\N
49	\\xc30d040703025a88e07e12e2f27b76d23a019831d4128fedf9d8181d4cc5dd3938c26189c3e9b5f5306b71f2a9c6464594d390673d98ea801a8cb742a6bf9ab001512f7c7a49ca56995051	\\xc30d040703024cf54c1ff2b69e1866d2440174991ac666542f37fa9460e0c2d8533fb8a3602addd3783f796313fe9cddf8e46414f59f2a54d259c216f1258ed7e1dec6a52b5432aef139b3a6671ed7a671d17cf089
50	\\xc30d04070302dd88b2a87d3c49087fd23a01c5d7681430396a029cb2c7078e4c05263c095211cc766de14186aa501e30a9fc04824e053dc0ec7a5c4776f24abb90438f48c57706c2b342bf	\\xc30d0407030207b5d92af64ac2876dd24201c5cb6e93cb157712c03ac8c66ae5ccbf1553b8c05a021ac5d9ae7663e869133439a6a3f791e2ac6b2e6c470864b932a901467119baec477e795021e16577c1e18b
51	\\xc30d04070302dc647a7e3d15be867cd23a018c518c2007d55f0b7310a746b6a67ce18fd70c2a296ab8cebd687d055e2bdd1c01135059b6f2ff934a759a89b150ac2e2237c11c5be942185a	\\xc30d04070302c1849d86d2388c0e70d246012368b9f96788866cd5b874246d0c29cc2056910f7998eb8e68a76bb3f073688079ea94a692dd6a2ba27a9151c46c2b67890236a02bb1e6424c66e74153e8d4be91e4ae251a
52	\\xc30d04070302c311f1e8ddb705d276d23a018ce708340aa33988c642116643df7f422e0e0fea2ddb457e5e9f2e7eeff63bd408d255a596e8d142740b9a8fba0f816ebf51769e445ce2a179	\\xc30d040703029779ec63b7a5387577d24901cb9838402bf96bbc8cb5df9d0c29c51e96c228952432439ec5bfedb9a0ee313ba905e9680fada5f8fdd94aa40b7b753a5d124bb34f85c8eea27b468c3fb3661f2b77dcca0ab457fc
53	\\xc30d0407030228c98e4c9c98ada66fd23a014931979d452d2218e09148f22b30b74cc23ceda709617a67829c7072d3d7878cf4d1ac7fa43785eb4ca57be935704eb272d4a384859335837e	\\xc30d04070302416ceff7a415a71a73d25101620128a86274f2e0f2cadc82086a16df42a9cedea5b54713186d29fc4fc840639bd791eef877ade23ea32f9ed425687fca47c6e14bddf0d8010db1d34f5ef452548cee870a3c28bc8a6e8321818de2ed
54	\\xc30d040703020d987af1475f314171d23a01ec68b5d23aff13c403a1dbc67f910cea5b5cde8daa3f00ffc8a03fa6a9dcf155e81f85e8d598d56ee7b9ff5c04aab09593576da7183cc8ddb3	\\xc30d0407030280453bdb8a23707e7fd24d015a490c30486562074671c2fb9a283af9543d311f21d6f4a17509b071c4f68050545d6c6043659a84b288646ad2400d1c66265947340f238de783f4fb7d7e92443df422310d512fd6d26d8ea8
55	\\xc30d0407030292048b6fbbfc9e0e7dd23a01e045fd4cc048e6d269336165b5d7231b1d072cd94a1ac298d6f3588041d588b4cecbb87e7aaa308bd3a40b5adeaa992e9197746617abe1ec5f	\\xc30d04070302a1fbfb554289ecd57ad24901fb520e9232ac7647b3c295d55d7444fc8dcdffc1308ca56f3460ca04c82d7b8df7af40952669d02d64b906dc59e5414bae20b0b90ee9a8a05192a91b0187c0b1e8f42e9a52dc3baa
56	\\xc30d04070302ad481aa5a3228c476fd23a013a96a507050992f9ad5d5af7b0d2da178d1cc3b663bd69367f0f4d2a3d8911adc8e8100717f0f9b828ba6cdf907e78d9121ee653cd728538db	\\xc30d04070302d44be0080067ff9c69d247010525447b1f4e42a22557d129f0bba8e4115f535d3bad909db6dd98f5173d290e9d929b0509bc069f47e1734635e8dde245d93b9c2636049e90bd7927bcaa69677b60dda7221a
57	\\xc30d04070302fb0c004da78ce08e61d23a0183398c5247841b722d8c62290b704dc20388a6199746cf2fb1f9fd9c8ee77dd520da645e316874e663ac32b0fddfcfc968dfa3736ce5f284b5	\\xc30d0407030254078299142d94dd6bd248017e9d557c6ce6ebb014abad62a27fd4fb9ec3e412a416421274efbab4a3165451fe27fb9cf626b867e18573919b7ba382812b07996d382843a10f25106955fd1ff6f444971f64e9
58	\\xc30d040703023455736de4ce79847fd23a012a5ed6092d1c3e3921b8e05aeab5c85005052aeb67d31631710d09eac0966edf00345d1d57c8dae802b8a4b7b62a4d472e4aab9d75728dac0d	\\xc30d04070302c35472600347997474d24601e6a076127872d46756e74f850ca204c0ee5e48cd79700d98eabc97a263c7c8c2e32d30a59cb3a6df60116787f0bb7bb6a9247f3dc988dbd3253388219993ac463c9f5b0f7f
59	\\xc30d040703025b2dc7bd7036c6a96ed23a0106965a621ccba2b7505d0518c0e2c2863af131195eeb0df06469b1807d69e8d6262cbe3f917e5a69e46b0777cbcc7418a929de3beb9953eebb	\\xc30d04070302f5e4eaf3889cb7bb72d246015dc772d5403e8a69a171948765b0ccd2c39dd1c566f70836769731b1d64c6cc69940f89bbe3ab23400a47575b8ede12236878648055008785f666649ffbc8e7623452094c4
60	\\xc30d04070302884960f1f09d5e367bd23a0151cf25fb2ec373712cc84b934c28983ea5c0c708a64f67a708767ea3ee99bf0856aa843ce321ba41f703b4b715e1df51a240ef6e0ff3848ed3	\\xc30d040703026fe9623cf0e0e75967d24a0159bd963705f72e3dda7757a92bc683d8bcd83edb760104dd609884f9aa425da5c3c4913c4fafb3068f28b5e76e9e8c76a10af357080cead42b0103d55286c54fb7f7d966d7c33fb76e
61	\\xc30d0407030277854975aa7da08875d23a019de1b2ad4c53b430e5b52dbc30cf0d56001adc0facfc5234a5103c0833652624d5b56698add86216a16786c3968618c2b78786d464cbfebde9	\\xc30d040703029eaf7b111284bf0b7dd24701d1ff4db76f9b69d0cb8c4fc4402966550aaf9cd9c16794ea3977fb7a2c9fc76869b1f242f73021820621d801b0f9d08f606cf9a3a4ff8e49071b382a1569cb925d298ea72350
62	\\xc30d0407030250f349e50187c95d66d23a0196ee62c63f00ee08459168c96714d107169e6229355a241563be3e1e3552f0e28209d64da9d464232bf14b148daa5d3ae0c3a6608b595bdb16	\\xc30d04070302d3de573da335595c64d2430149716d27cd38451cb27b87085e28325a9b7130d78adca18871f320f1615023c4ace8bab0702b5597032c7821435bef8cee0cedc6e7dc24b2f872e839d9cc7b92633c
63	\\xc30d04070302c220e1b7055c410866d23a01fb3ae2888a84a51a1a9b946929970589fee27b2b0179787888bc584053268cc910f04c20af47abdfb9ab95c37be713175c76bf77fe79ce8c98	\\xc30d04070302bf03987288c05c3a65d23f015ac4ff874b7f8921b53602a8ccff0a8f2a900a620e0aa972ee64124eccfe0ad47b6b5a0f70c6990e43a5776d58595ca31ea3ac4cf6b318e2a0cb7550cecf
64	\\xc30d040703029e6cadf5275b964367d23a01d61be865d0d74964f02c9f1b46e1491f9b9e621b54546957ffaecce7d3d0b197fda5dbd02507d71384f3ba41564655dcd6fe46a3fb1c090642	\\xc30d0407030206aff0a16432681f68d244010ec85ea47f4ad84fd69218021b34ac26837d99479e445e5baffd2b1eec79190fe266a992ebaf5e69123d95c81cb501c8cffb54866627b8f85f88aa54c1227605ee5621
65	\\xc30d040703024967b310ea6c40d474d23a01b4d2d98585d9ddfc06efe1d8135ec30d9aa981073cae6096c1459505faddbaebe704a85f5b66e184edbd41ab9522f2c4aebc3bfaa729258cb9	\\xc30d04070302c055d6185710775962d2470152b91ae5a691d6114fafe0cc2d546b695e09804089ac4021dcbe089d4dc6d9b1d4fd256cd2bab6638a441e74e95e181375b07070d082f3d295fdd6268b700391c7a157df2c89
66	\\xc30d04070302577ec182eaed62207dd23a01841d9a5a4ba877374f3300fa407e0e6796c949dd3d533da732aff116a74da092fbb1b59604551488943039bae7e5bba4a662cfa33469b3e163	\\xc30d040703029f9daa66559b44b572d2470177787123900ed60030f810f5a26edeb126d7311c8804792e99c1d8c3f9d6ed281f5c00fafd048f628b81c46249da5fcaa4e5c5affcdef5fc1b99744a67622110a6087b3c3788
67	\\xc30d040703024217261c7c331ff563d23a012cbb46a02d6fb59da02f48df4aef45e95dd9e42675a31dc687af4732477d6adfcc4d0ec90a8b63c2a18b1226994c7071ee9af5e96c0035745b	\\xc30d040703028397652dd10c6a9f62d24401e630292639d8049ba95359ae12f60796cf09054a88a2d1d9293e9e1b158a3b7aa63fadea4cddf0a147bd33f9e98fa8e287242366bf51e7822de6dffa19fe00e5829089
68	\\xc30d0407030213fb805701258cb979d23a01491b95470f26216748a9e4c6f7fa1f56bfd38a745f7a1857f224ac763a0a96d039308f7be0ae895056392182bd7b9547e291fff4cd2a96f605	\\xc30d0407030287c214ad5351a53271d245018612cd55c912200f4d0954d8c560422bab408a5b899a602e67624214a2d6d21bc900f5e87278162e56cb1b09a087402c0c64d97b6ee7098114c2954a7f089d0707b58727
69	\\xc30d04070302c5f5578e504a90ee6dd23a011884dfddbb8c18068fed0a8c1253c805e9e3f7c5e420886e4eacbccd0ea1cdbf15b775242b444da2ef43726bb6fb1a9870af69c0fc58962683	\\xc30d04070302290d6a1f8d6a4eff65d246017f23c0fbad9097b03a831f725ea45396575b6b8e7243b7ff6f206543e9f760c2abfa463c47011423e25236c83e04b12e05572a2edb05d90d8d195e59cd626f8519bd88b2aa
70	\\xc30d040703020bf0de6824c7a66469d23a01f8bd9bee9a2804c1f853f5e5dfdf8c2b0b00b4822a7f283f5b8d2f1bbd1b477950dc687aeb0c4241ac6b2d4527af6721f869c6d8d7a31ee7a0	\\xc30d04070302dd8120d1f54e2c5976d24601b0a1a1c23cac38803af3ba80ee06f80eab35d3e2fdc1b40304c476b687e1fe68c2f716f2a3f829a85f6f8a20de7422a99d82aff6ac94aa964e5bc0edf6336c088d9739357d
71	\\xc30d040703026c80be740b702bfc7dd23a01052fa122bcba969ea68e126f74039b02b7af1c7f19d29165f7a374ae40146c7d981c8a28ee5f09de0932d2ba401a49198f6822b15dfd506c05	\\xc30d040703029b038f833433b29d79d249015860e9c9a6c0ef2928d2663fb566e4722f6c380f7b0b0d812989d87e1039fc7957237ac9cace5f911951cc28f92e4c3af71fcdc14c32796950ecf75ef4710e0a9c5ebd845a7b3245
72	\\xc30d04070302c815a3649c86b1516ad23a0196c1c787a097c2c7c9949b914823926df072450716686016f506f79bf3a3024e2c6dc2626c4a93badc1d2d610b5cf79bf0247d2a5e320bc4e9	\\xc30d040703022519d6e61f8df08061d248015608ea0ee9b723461693f579191f557089dde843969c0ab88b2f9f0f45c82d1999dbbed4719dfe90084d189d200b659860dd78b80905e19daa0eda7f2b57d30214381729544c2f
73	\\xc30d04070302ddf5dad61fc24ee568d23a018725e3730cf49b3c24f383f7c5c1f180d4db494d77fcdea4cd66212d8960490cfadf9796ed2a15e3d45cdb326abef625062eb0fcea26e23e49	\\xc30d04070302b300d20abc06f9ae6ad24201b303a185d44f4b5f839d88caad20b30ec85fbfb13eaffeebc4c607f2e7314bac3a35fb59b7af9c83cce38ba863297d04fb04c0c80bca3a95888fb810b93b825c6b
74	\\xc30d040703023ae4f3b0760d8d036ad23a0116c64df65812f58a7b80b6f5a3ff946a7a76b91eca9bbfe2ee30755702eea51059fb7f7cfd57cd53aa161869c90e8dd3c1829c890c4e00f794	\\xc30d040703024c007ad27f247a1f7ad24701e3310b08929c61fdc4b57e82ae82d3583ef3006a582b7667b926f5329321ff5037937df71bc7142b97f1a0eb63c2eaa593b5ecff826fe07fd85f4f6cf4d94632b523039a683b
75	\\xc30d040703025ea062fbdea4707975d23a0159506f1f32cfdfa092d2a5963840244ef9b58f9d0a603454364f393713da5a465ef5bc78b2a7f2218262b5a69d0e91e051d83ec850ee8b0319	\\xc30d040703020ab4a9e569c6f5d57dd2460165fda5524d12d6327b0d145db9a8d82e887d17449bc9f5608f2b79d9fadec6fcafefefd510444ec2cd300756f2d9ad50231489e557e825e0865222a6c5a2badc34a5816594
76	\\xc30d04070302ed910e666bc565116bd23a01e3b941e2f50f1315c552c3c42bc44a0e0213c35970befdb83cc87b241811a1057842ad67da0c0e2c63cf73b9d397a5cda37810a7506707e93e	\\xc30d04070302fc37e0548acf035769d24401a1e4651341f0dd94f6e772ceb00b950860bb1f7409685409bd3ac67afa3d7a29b0f8cd049aa870eaacde0cc8e9579a6e69193212f20e1360ef2bd67f096a09bc618f09
77	\\xc30d040703020c79cf2c00bcabc661d23a012a6db7085b50ba20e3c3e403b8b6dd25221018c2ae80e3fe637008c1f10cde3b3b70054b8aff7cfa40bc74e6dedf8d346bc81b5954f6765af4	\\xc30d04070302f37c524043b6584b6dd24801de8e89fc0fd05252ef388f08cce4431ac3690d0287880d99fdffd4c4f4b5a02b610c89d4c2bd2e80d0bf0acdb6dcc3f6aaf93e334eed3dcad490a6e5d22f04875f976391f56899
78	\\xc30d040703023047bb1ef60ebc2e7fd23a01999948743b57ff22eb4d21e7ca21ff9c865c573921216661c28089e55537469d4f93795d6deb6676eef01f635f33f09be79235e9dd6aa6d99f	\\xc30d04070302b7c246b0ce18e7a160d24701d1783d3a974e530fee1ca63752d5909ae1daeeb7927db5d8a92d42795db5e0fee0f6ec1f3395eec7d036fc68ea842e4f50e81e1afd02dbf304c0b3128f95a49ebe3d2dd637bc
79	\\xc30d040703027946de4ada264d1b6bd23a0172fd15541af9ab2edd62a7c333744c0253d3d8bac92bac9b313bac185fd1aa6c116af565acd761cf887d9f7918b6dbed2fe2992082d066aeb9	\\xc30d0407030204f9d4fafab6e2b27dd24601c3426f89c38c1a8f510f9a16f83e1fb186d61b0b2e0845575431a427b7aa7ff9ffad6ce071bda981da9b315b3d1b1738d28ea59b97d079426f50b158779e4e9fe67e1458ee
30	\\xc30d040703029cc9f5264815226b7cd23a014fe0e9fd8abfa2dfb65e4c07687d105f882c5f2dfb903d94d39b554343969bb3d5cc916837ceebbefdf9070ef4037817a1136339e6f88cfaec	\\xc30d04070302371f9f35caee25ea69d241018a4b336d05638750ea34f379a31b2d846fc2e306453d0f091e73d9b2f91eefe42c24127f55dd19486110ad9eed7368136ac6ed6e42a8febdf0d1de2cc8e4ebcf
1	\\xc30d040703029883d6a650c551db65d23a0178bdf99b7e1d4bde418b613d53067e5aa2fd9618e66e4f7780ff43e3dccb1cd9b48000431fcc8e789b6baf04250a2d5e2cb9335349ac3cd727	\\xc30d0407030253e5b3656895869e60d241019d0b8b767f81f74daed4ac07f0b1b718454b2c374daac0f1c89b2b6660bfb98f8653c839b5387d8686ea762b32ef681d5234ee416aa9e5fca75a9dddef74e492
\.


--
-- TOC entry 3539 (class 0 OID 25230)
-- Dependencies: 230
-- Data for Name: department; Type: TABLE DATA; Schema: hospital; Owner: postgres
--

COPY hospital.department (department_id, department, floor, room) FROM stdin;
1	chirurgie	3	\N
2	interna	6	\N
3	ortopedie	7	\N
4	interna	5	\N
5	gynokologie	7	\N
6	psychiatrie	1	\N
7	neurologie	4	\N
8	pneumonologie	2	\N
9	hematologie	10	\N
10	urologie	5	\N
11	chirurgie	4	\N
\.


--
-- TOC entry 3534 (class 0 OID 25204)
-- Dependencies: 225
-- Data for Name: diagnose; Type: TABLE DATA; Schema: hospital; Owner: postgres
--

COPY hospital.diagnose (diagnose_id, diagnose) FROM stdin;
1	akutní apendicitida
2	nekompenzovaný DM II. typu
3	TEP dextra genus
4	gravidita
5	schizofrenie
6	CMP
7	TBC
8	leukemie
9	renální kolika
10	nefrolithiáza
11	tříselná kýla
12	fraktura femuru
\.


--
-- TOC entry 3529 (class 0 OID 25170)
-- Dependencies: 220
-- Data for Name: doctor; Type: TABLE DATA; Schema: hospital; Owner: postgres
--

COPY hospital.doctor (doctor_id, doctor_name, doctor_surname, contact_id) FROM stdin;
1	Petr	Solař	52
2	Helena	Randulová	53
3	Julie	Vondráčková	54
4	Šimon	Velký	55
5	Miroslav	Rychlý	56
6	Pavel	Nárožný	57
7	Hana	Nováková	58
8	Lenka	Modrá	59
9	Jonáš	Černý	60
10	Michal	Zeman	61
11	Marcela	Kočárova	62
12	Zikmund	Srna	63
13	Václav	Čmelařík	64
14	Edita	Navrátilová	65
15	Simona	Součková	66
16	Vlastimil	Ořech	67
17	Drahomíra	Svatavská	68
18	Aleš	Komárek	69
19	Pavel	Martinásek	70
20	Vlasta	Holešová	71
21	Martin	Švestka	72
22	Martina	Krásná	73
23	Anežka	Brothánková	74
24	Ester	Krátošková	75
25	Milada	Horská	76
26	Karel	Pavlíček	77
27	Marta	Berková	78
28	Martin	Berka	79
\.


--
-- TOC entry 3558 (class 0 OID 26151)
-- Dependencies: 251
-- Data for Name: dummy1; Type: TABLE DATA; Schema: hospital; Owner: postgres
--

COPY hospital.dummy1 (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) FROM stdin;
\.


--
-- TOC entry 3557 (class 0 OID 26148)
-- Dependencies: 250
-- Data for Name: dummy2; Type: TABLE DATA; Schema: hospital; Owner: postgres
--

COPY hospital.dummy2 (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) FROM stdin;
\.


--
-- TOC entry 3550 (class 0 OID 25283)
-- Dependencies: 241
-- Data for Name: hospital; Type: TABLE DATA; Schema: hospital; Owner: postgres
--

COPY hospital.hospital (hospital_id, hospital_name, address_id) FROM stdin;
1	Vojenská nemocnice Brno	52
2	Nemocnice České Budějovice	53
3	Fakultní nemocnice Hradec Králové	54
4	Nemocnice Jihlava	55
5	Oblastní nemocnice Kladno	56
6	Krajská nemocnice Liberec	57
7	Nemocnice Most	58
8	Fakultní nemocnice Olomouc	59
9	Fakultní nemocnice Ostrava	60
10	Slezská nemocnice v Opavě	61
11	Pardubická nemocnice	62
12	Fakultní nemocnice Plzeň	63
13	Všeobecná fakultní nemocnice v Praze	64
14	Krajská nemocnice Tomáše Bati Zlín	65
\.


--
-- TOC entry 3553 (class 0 OID 25299)
-- Dependencies: 244
-- Data for Name: hospital_has_patient; Type: TABLE DATA; Schema: hospital; Owner: postgres
--

COPY hospital.hospital_has_patient (hospital_id, patient_id) FROM stdin;
1	2
1	9
1	22
1	23
1	26
1	32
1	41
1	50
2	16
2	19
2	35
3	18
4	5
4	27
4	34
4	47
4	49
5	4
5	29
6	14
6	15
7	13
7	28
7	38
7	46
8	17
8	20
8	42
9	6
9	43
9	45
10	21
10	37
11	3
11	12
11	39
12	1
12	11
12	36
13	8
13	10
13	25
13	31
13	40
13	44
13	48
14	7
14	24
14	33
\.


--
-- TOC entry 3523 (class 0 OID 25141)
-- Dependencies: 214
-- Data for Name: insurance_company; Type: TABLE DATA; Schema: hospital; Owner: postgres
--

COPY hospital.insurance_company (insurance_company_id, insurance_company, insurance_company_code) FROM stdin;
1	Všeobecná zdravotní pojišťovna ČR	111
2	Vojenská zdravotní pojišťovna ČR	201
3	Česká průmyslová zdravotní pojišťovna	205
4	Oborová zdravotní pojišťovna	207
5	Zaměstnanecká pojišťovna Škoda	209
6	Zdravotní pojišťovna ministerstva vnitra ČR	211
7	RBP, zdravotní pojišťovna	213
\.


--
-- TOC entry 3526 (class 0 OID 25149)
-- Dependencies: 217
-- Data for Name: patient; Type: TABLE DATA; Schema: hospital; Owner: postgres
--

COPY hospital.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) FROM stdin;
1	Erik	Hrbek	1983-06-18	830618/0993	1	5
2	Šimon	Pejša	1983-05-09	830509/9044	2	4
4	Evženie	Klabnová	1999-03-03	990303/3745	4	2
5	Vlastislav	Martinec	2000-12-06	001206/0092	5	1
8	Denisa	Brodská	2000-09-21	005921/6069	8	3
10	Zlata	Holoubková	1983-03-15	835315/7065	10	7
11	Evelína	Fryčová	1968-02-12	685212/4213	11	5
13	Simona	Vacková	1996-05-03	965503/0363	13	2
14	Eva	Patáková	1974-05-20	745620/7011	14	1
16	Hubert	Benda	1980-09-13	800913/3858	16	1
17	Servác	Rusek	1976-09-14	760914/9955	17	2
18	Kvido	Vašíček	1975-09-20	750920/0193	18	3
19	Bořek	Chromý	1954-01-27	540127/2525	19	5
21	Otakar	Večerka	1959-05-02	590502/2024	21	6
22	Igor	Grepl	1977-10-22	771022/7998	22	7
23	Teodor	Pilát	2006-10-23	061023/0071	23	5
24	Luboš	Vobořil	1962-04-03	620403/6817	24	3
25	Viktorie	Štefanová	1959-06-18	595618/2760	25	1
26	Alena	Jiříková	1992-07-04	925704/3653	26	3
27	Marina	Halušková	1968-05-17	685517/9397	27	1
28	Xenie	Koubková	2003-03-26	035326/8003	28	2
29	Gita	Becková	1977-04-03	775403/4024	29	6
31	Radka	Zelená	1955-01-07	555107/1141	31	1
32	Blažena	Schusterová	1992-07-22	925722/2909	32	1
33	Ivona	Plašilová	2005-07-11	055711/4360	33	3
34	Eduard	Holec	1979-01-25	790125/8112	34	2
35	Zbyšek	Stříž	1973-03-19	730319/2127	35	2
36	Přemysl	Štefl	1996-10-02	961002/6129	36	4
37	Ignác	Halíř	1961-02-13	610213/2289	37	4
38	Vojtěch	Kubín	1981-04-17	810417/5970	38	5
39	Rostislav	Zachoval	2001-04-11	010411/1579	39	6
40	David	Hradský	1986-07-28	860728/6215	40	7
41	Vratislav	Řihák	1964-08-06	640806/2661	41	7
42	Pravoslav	Vyoral	2002-02-19	020219/5059	42	2
43	Julius	Kavan	1966-03-07	660307/5798	43	1
44	Vlastislav	Pour	1974-11-22	741122/0267	44	4
45	Richard	Štefek	1972-02-16	720216/8171	45	5
46	Miluše	Daníčková	1959-04-22	595422/1273	46	6
47	Žaneta	Krupičková	2008-07-14	085714/8655	47	2
48	Gizela	Kinclová	1976-12-08	766208/0547	48	4
49	Vendula	Kopečková	1991-07-03	915703/9628	49	3
50	Drahoslava	Bělíková	1992-05-17	925517/7998	50	1
3	Leoš	Sikula	2017-09-01	170901/2316	3	1
9	Magdaléna	Klásková	1962-11-11	626111/3067	9	6
20	Václav	Hrbek	2009-09-23	090923/7604	20	4
6	Marcel	Šťovíček	2012-08-05	120805/5882	6	1
7	Renáta	Kalinová	2012-12-13	126213/4742	7	2
15	Michaela	Pavlovská	1975-06-18	755618/2359	15	3
12	Julie	Šoltysová	1993-10-16	936016/0601	12	4
\.


--
-- TOC entry 3547 (class 0 OID 25264)
-- Dependencies: 238
-- Data for Name: patient_has_address; Type: TABLE DATA; Schema: hospital; Owner: postgres
--

COPY hospital.patient_has_address (patient_id, address_id) FROM stdin;
1	1
2	2
3	3
4	4
5	5
6	6
7	7
8	8
9	9
10	10
11	11
12	12
13	13
14	14
15	15
16	16
17	17
18	18
19	19
20	20
21	21
22	22
23	23
24	24
25	25
26	26
27	27
28	28
29	29
31	31
32	32
33	33
34	34
35	35
36	36
37	37
38	38
39	39
40	40
41	41
42	42
43	43
44	44
45	45
46	46
47	47
48	48
49	49
50	50
\.


--
-- TOC entry 3537 (class 0 OID 25212)
-- Dependencies: 228
-- Data for Name: patient_has_diagnose; Type: TABLE DATA; Schema: hospital; Owner: postgres
--

COPY hospital.patient_has_diagnose (patient_id, diagnose_id) FROM stdin;
1	2
2	2
3	1
4	4
5	5
6	3
7	6
8	7
9	8
10	9
11	11
12	12
13	9
14	12
15	8
16	9
17	3
18	11
19	7
20	5
21	2
22	2
23	8
24	6
25	7
26	8
27	11
28	9
29	4
31	9
32	2
33	4
34	5
35	7
36	12
37	12
38	9
39	8
40	9
41	2
42	3
43	7
44	7
45	7
46	4
47	1
48	7
49	5
50	2
\.


--
-- TOC entry 3532 (class 0 OID 25186)
-- Dependencies: 223
-- Data for Name: patient_has_doctor; Type: TABLE DATA; Schema: hospital; Owner: postgres
--

COPY hospital.patient_has_doctor (patient_id, doctor_id) FROM stdin;
1	3
2	1
3	2
4	5
5	6
6	4
7	7
8	8
9	9
10	10
11	3
12	2
13	11
14	23
15	24
16	19
17	25
18	21
19	20
20	26
21	27
22	1
23	9
24	14
25	8
26	9
27	15
28	11
29	5
31	10
32	1
33	7
34	6
35	20
36	3
37	27
38	11
39	13
40	10
41	1
42	25
43	17
44	8
45	17
46	12
47	15
48	8
49	6
50	1
\.


--
-- TOC entry 3542 (class 0 OID 25238)
-- Dependencies: 233
-- Data for Name: patient_in_department; Type: TABLE DATA; Schema: hospital; Owner: postgres
--

COPY hospital.patient_in_department (patient_id, department_id, date) FROM stdin;
1	2	2015-02-23 00:00:00
2	4	2021-09-20 00:00:00
3	1	2018-12-02 00:00:00
5	6	2021-10-25 00:00:00
6	3	2020-03-05 00:00:00
8	8	2021-08-10 00:00:00
9	9	2021-05-11 00:00:00
10	10	2021-03-12 00:00:00
11	11	2021-07-27 00:00:00
12	1	2021-01-23 00:00:00
13	10	2020-12-23 00:00:00
14	4	2021-01-02 00:00:00
15	9	2021-04-20 00:00:00
16	10	2021-07-03 00:00:00
17	3	2021-09-14 00:00:00
18	11	2020-08-01 00:00:00
19	8	2021-05-23 00:00:00
21	4	2021-01-09 00:00:00
22	2	2021-09-04 00:00:00
23	9	2021-07-02 00:00:00
24	5	2021-04-30 00:00:00
26	9	2021-03-12 00:00:00
27	1	2021-06-18 00:00:00
28	10	2021-01-27 00:00:00
29	5	2021-06-08 00:00:00
31	10	2021-04-14 00:00:00
32	2	2021-01-03 00:00:00
33	7	2021-01-23 00:00:00
34	6	2021-01-29 00:00:00
35	8	2021-05-01 00:00:00
36	4	2021-04-03 00:00:00
37	4	2020-11-23 00:00:00
38	10	2020-12-30 00:00:00
39	9	2021-01-01 00:00:00
40	10	2020-12-27 00:00:00
41	2	2020-11-12 00:00:00
42	3	2021-04-21 00:00:00
43	8	2021-08-11 00:00:00
44	8	2021-04-13 00:00:00
45	8	2021-07-19 00:00:00
46	5	2021-05-11 00:00:00
47	1	2021-01-28 00:00:00
48	8	2021-02-20 00:00:00
49	6	2021-08-10 00:00:00
50	2	2021-06-05 00:00:00
20	6	2021-11-14 00:00:00
7	7	2021-11-15 00:00:00
4	5	2021-10-01 00:00:00
25	8	2021-10-31 00:00:00
\.


--
-- TOC entry 3556 (class 0 OID 26101)
-- Dependencies: 249
-- Data for Name: user; Type: TABLE DATA; Schema: hospital; Owner: postgres
--

COPY hospital."user" (user_id, username, password, name, surname) FROM stdin;
1	user1	$2a$12$Xh9m2x7Ks6Vu3yYPJ8y2j.noJ1lVf5nPH5pvYQ9WKmKOvkEqsmxmW	\\xc30d0407030206f10c9dce6f377069d2350190e44befc1bb4f5c9f7461521383ed1b2f4f1dba4e2b0db063c9d564765595da2ed4939b037b83c77ab14cc9e859aa8f6bb3aba8	\\xc30d040703022167d6b328db57c662d2370138b32afceeff888bd3cbebd14d5f52f6a3d59a3f1967463fc87c2b3b836594c6ecc4db26852f8ea46c7424a5c675d80a8cf75c2d1b75
2	user2	$2a$12$Iq2FvO/P5uD4jGqGuY/ZsuvJULV0pRuML/OW5TxcBhnitAFWjXdCO	\\xc30d040703022f964563b4d7cd9077d23501ebdefe81aca53924605adfb418b9a68a095a13d4bc00482f18061c7f07c57dc26344289439bc7e0cf9a8209646bde1702def105a	\\xc30d040703020221b78a35c0985d71d23801949bfc0225e9ca6eababa0ec89b35eab5c06df889f8c50ac7adf2c1ae589aa83e17e8fba21ed977f47e646d608c8ba8f949a000ca3abce
3	user3	$2a$12$zcAxTcHwk6gUckl/s2PpgOd5M1lArsu9pqYjWszvBVT99oZqsa5Xe	\\xc30d04070302e233ca07c6a87a5963d23401881616c00d137ede5b954c7b65c71d6e2ac3122cdd1a9cdf1485e7007587d3a01db7de684e52568a80710f71d363b1bbc63314	\\xc30d040703027ff4c6fb4bc0123671d23701fb317f477b8f29c5f354e478bc37c395588837bce49e2d2fcb2b134f8ebef60c7f8b1bb2b2115b635505de9bca6c6f6706fd97da7e2b
4	user4	$2a$12$jjiV74YpkswD4M7BFm/8VOxZ2R7M8ov8LlxYfIs39lShHEJKuvIIG	\\xc30d04070302e7c09f1f968c848060d23401c817aefccc6543882e0b45af5c8b57770f4d1f98497a7dec4f4a7269ca0edcc682a5afe1457d5ab5634c55a20bb54f9e730130	\\xc30d0407030277b97c7efd70391863d2370120ad6aa84f0397caba5cc0ac153ad615c5a09576847d82f5d47717a6d7aea65321be44bbfa99ddf66f459840d6959f427de0e4e9f14d
\.


--
-- TOC entry 3630 (class 0 OID 0)
-- Dependencies: 234
-- Name: address_address_id_seq; Type: SEQUENCE SET; Schema: hospital; Owner: postgres
--

SELECT pg_catalog.setval('hospital.address_address_id_seq', 65, true);


--
-- TOC entry 3631 (class 0 OID 0)
-- Dependencies: 211
-- Name: contact_contact_id_seq; Type: SEQUENCE SET; Schema: hospital; Owner: postgres
--

SELECT pg_catalog.setval('hospital.contact_contact_id_seq', 79, true);


--
-- TOC entry 3632 (class 0 OID 0)
-- Dependencies: 229
-- Name: department_department_id_seq; Type: SEQUENCE SET; Schema: hospital; Owner: postgres
--

SELECT pg_catalog.setval('hospital.department_department_id_seq', 11, true);


--
-- TOC entry 3633 (class 0 OID 0)
-- Dependencies: 224
-- Name: diagnose_diagnose_id_seq; Type: SEQUENCE SET; Schema: hospital; Owner: postgres
--

SELECT pg_catalog.setval('hospital.diagnose_diagnose_id_seq', 16, true);


--
-- TOC entry 3634 (class 0 OID 0)
-- Dependencies: 219
-- Name: doctor_contact_id_seq; Type: SEQUENCE SET; Schema: hospital; Owner: postgres
--

SELECT pg_catalog.setval('hospital.doctor_contact_id_seq', 1, false);


--
-- TOC entry 3635 (class 0 OID 0)
-- Dependencies: 218
-- Name: doctor_doctor_id_seq; Type: SEQUENCE SET; Schema: hospital; Owner: postgres
--

SELECT pg_catalog.setval('hospital.doctor_doctor_id_seq', 28, true);


--
-- TOC entry 3636 (class 0 OID 0)
-- Dependencies: 240
-- Name: hospital_address_id_seq; Type: SEQUENCE SET; Schema: hospital; Owner: postgres
--

SELECT pg_catalog.setval('hospital.hospital_address_id_seq', 1, false);


--
-- TOC entry 3637 (class 0 OID 0)
-- Dependencies: 242
-- Name: hospital_has_patient_hospital_id_seq; Type: SEQUENCE SET; Schema: hospital; Owner: postgres
--

SELECT pg_catalog.setval('hospital.hospital_has_patient_hospital_id_seq', 1, false);


--
-- TOC entry 3638 (class 0 OID 0)
-- Dependencies: 243
-- Name: hospital_has_patient_patient_id_seq; Type: SEQUENCE SET; Schema: hospital; Owner: postgres
--

SELECT pg_catalog.setval('hospital.hospital_has_patient_patient_id_seq', 1, false);


--
-- TOC entry 3639 (class 0 OID 0)
-- Dependencies: 239
-- Name: hospital_hospital_id_seq; Type: SEQUENCE SET; Schema: hospital; Owner: postgres
--

SELECT pg_catalog.setval('hospital.hospital_hospital_id_seq', 14, true);


--
-- TOC entry 3640 (class 0 OID 0)
-- Dependencies: 213
-- Name: insurance_company_insurance_company_id_seq; Type: SEQUENCE SET; Schema: hospital; Owner: postgres
--

SELECT pg_catalog.setval('hospital.insurance_company_insurance_company_id_seq', 7, true);


--
-- TOC entry 3641 (class 0 OID 0)
-- Dependencies: 216
-- Name: patient_contact_id_seq; Type: SEQUENCE SET; Schema: hospital; Owner: postgres
--

SELECT pg_catalog.setval('hospital.patient_contact_id_seq', 1, false);


--
-- TOC entry 3642 (class 0 OID 0)
-- Dependencies: 237
-- Name: patient_has_address_address_id_seq; Type: SEQUENCE SET; Schema: hospital; Owner: postgres
--

SELECT pg_catalog.setval('hospital.patient_has_address_address_id_seq', 1, false);


--
-- TOC entry 3643 (class 0 OID 0)
-- Dependencies: 236
-- Name: patient_has_address_patient_id_seq; Type: SEQUENCE SET; Schema: hospital; Owner: postgres
--

SELECT pg_catalog.setval('hospital.patient_has_address_patient_id_seq', 1, false);


--
-- TOC entry 3644 (class 0 OID 0)
-- Dependencies: 227
-- Name: patient_has_diagnose_diagnose_id_seq; Type: SEQUENCE SET; Schema: hospital; Owner: postgres
--

SELECT pg_catalog.setval('hospital.patient_has_diagnose_diagnose_id_seq', 1, false);


--
-- TOC entry 3645 (class 0 OID 0)
-- Dependencies: 226
-- Name: patient_has_diagnose_patient_id_seq; Type: SEQUENCE SET; Schema: hospital; Owner: postgres
--

SELECT pg_catalog.setval('hospital.patient_has_diagnose_patient_id_seq', 1, false);


--
-- TOC entry 3646 (class 0 OID 0)
-- Dependencies: 222
-- Name: patient_has_doctor_doctor_id_seq; Type: SEQUENCE SET; Schema: hospital; Owner: postgres
--

SELECT pg_catalog.setval('hospital.patient_has_doctor_doctor_id_seq', 1, false);


--
-- TOC entry 3647 (class 0 OID 0)
-- Dependencies: 221
-- Name: patient_has_doctor_patient_id_seq; Type: SEQUENCE SET; Schema: hospital; Owner: postgres
--

SELECT pg_catalog.setval('hospital.patient_has_doctor_patient_id_seq', 1, false);


--
-- TOC entry 3648 (class 0 OID 0)
-- Dependencies: 232
-- Name: patient_in_department_department_id_seq; Type: SEQUENCE SET; Schema: hospital; Owner: postgres
--

SELECT pg_catalog.setval('hospital.patient_in_department_department_id_seq', 1, false);


--
-- TOC entry 3649 (class 0 OID 0)
-- Dependencies: 231
-- Name: patient_in_department_patient_id_seq; Type: SEQUENCE SET; Schema: hospital; Owner: postgres
--

SELECT pg_catalog.setval('hospital.patient_in_department_patient_id_seq', 1, false);


--
-- TOC entry 3650 (class 0 OID 0)
-- Dependencies: 215
-- Name: patient_patient_id_seq; Type: SEQUENCE SET; Schema: hospital; Owner: postgres
--

SELECT pg_catalog.setval('hospital.patient_patient_id_seq', 67, true);


--
-- TOC entry 3651 (class 0 OID 0)
-- Dependencies: 248
-- Name: user_user_id_seq; Type: SEQUENCE SET; Schema: hospital; Owner: postgres
--

SELECT pg_catalog.setval('hospital.user_user_id_seq', 15, true);


--
-- TOC entry 3352 (class 2606 OID 25261)
-- Name: address address_pkey; Type: CONSTRAINT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.address
    ADD CONSTRAINT address_pkey PRIMARY KEY (address_id);


--
-- TOC entry 3328 (class 2606 OID 25139)
-- Name: contact contact_pkey; Type: CONSTRAINT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.contact
    ADD CONSTRAINT contact_pkey PRIMARY KEY (contact_id);


--
-- TOC entry 3348 (class 2606 OID 25235)
-- Name: department department_pkey; Type: CONSTRAINT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.department
    ADD CONSTRAINT department_pkey PRIMARY KEY (department_id);


--
-- TOC entry 3344 (class 2606 OID 25209)
-- Name: diagnose diagnose_pkey; Type: CONSTRAINT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.diagnose
    ADD CONSTRAINT diagnose_pkey PRIMARY KEY (diagnose_id);


--
-- TOC entry 3338 (class 2606 OID 25178)
-- Name: doctor doctor_doctor_id_key; Type: CONSTRAINT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.doctor
    ADD CONSTRAINT doctor_doctor_id_key UNIQUE (doctor_id);


--
-- TOC entry 3340 (class 2606 OID 25176)
-- Name: doctor doctor_pkey; Type: CONSTRAINT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.doctor
    ADD CONSTRAINT doctor_pkey PRIMARY KEY (doctor_id, contact_id);


--
-- TOC entry 3360 (class 2606 OID 25305)
-- Name: hospital_has_patient hospital_has_patient_pkey; Type: CONSTRAINT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.hospital_has_patient
    ADD CONSTRAINT hospital_has_patient_pkey PRIMARY KEY (hospital_id, patient_id);


--
-- TOC entry 3356 (class 2606 OID 25291)
-- Name: hospital hospital_hospital_id_key; Type: CONSTRAINT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.hospital
    ADD CONSTRAINT hospital_hospital_id_key UNIQUE (hospital_id);


--
-- TOC entry 3358 (class 2606 OID 25289)
-- Name: hospital hospital_pkey; Type: CONSTRAINT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.hospital
    ADD CONSTRAINT hospital_pkey PRIMARY KEY (hospital_id, address_id);


--
-- TOC entry 3331 (class 2606 OID 25146)
-- Name: insurance_company insurance_company_pkey; Type: CONSTRAINT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.insurance_company
    ADD CONSTRAINT insurance_company_pkey PRIMARY KEY (insurance_company_id);


--
-- TOC entry 3354 (class 2606 OID 25270)
-- Name: patient_has_address patient_has_address_pkey; Type: CONSTRAINT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.patient_has_address
    ADD CONSTRAINT patient_has_address_pkey PRIMARY KEY (patient_id, address_id);


--
-- TOC entry 3346 (class 2606 OID 25218)
-- Name: patient_has_diagnose patient_has_diagnose_pkey; Type: CONSTRAINT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.patient_has_diagnose
    ADD CONSTRAINT patient_has_diagnose_pkey PRIMARY KEY (diagnose_id, patient_id);


--
-- TOC entry 3342 (class 2606 OID 25192)
-- Name: patient_has_doctor patient_has_doctor_pkey; Type: CONSTRAINT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.patient_has_doctor
    ADD CONSTRAINT patient_has_doctor_pkey PRIMARY KEY (patient_id, doctor_id);


--
-- TOC entry 3350 (class 2606 OID 25244)
-- Name: patient_in_department patient_in_department_pkey; Type: CONSTRAINT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.patient_in_department
    ADD CONSTRAINT patient_in_department_pkey PRIMARY KEY (patient_id, department_id);


--
-- TOC entry 3334 (class 2606 OID 25157)
-- Name: patient patient_patient_id_key; Type: CONSTRAINT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.patient
    ADD CONSTRAINT patient_patient_id_key UNIQUE (patient_id);


--
-- TOC entry 3336 (class 2606 OID 25155)
-- Name: patient patient_pkey; Type: CONSTRAINT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.patient
    ADD CONSTRAINT patient_pkey PRIMARY KEY (patient_id, contact_id, insurance_company_id);


--
-- TOC entry 3362 (class 2606 OID 26106)
-- Name: user user_pkey; Type: CONSTRAINT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (user_id);


--
-- TOC entry 3329 (class 1259 OID 25927)
-- Name: idx1; Type: INDEX; Schema: hospital; Owner: postgres
--

CREATE INDEX idx1 ON hospital.insurance_company USING btree (insurance_company);


--
-- TOC entry 3332 (class 1259 OID 25980)
-- Name: idx_surname; Type: INDEX; Schema: hospital; Owner: postgres
--

CREATE INDEX idx_surname ON hospital.patient USING btree (surname);


--
-- TOC entry 3377 (class 2620 OID 25941)
-- Name: patient young; Type: TRIGGER; Schema: hospital; Owner: postgres
--

CREATE TRIGGER young AFTER INSERT ON hospital.patient FOR EACH ROW EXECUTE FUNCTION hospital.young_patient();


--
-- TOC entry 3365 (class 2606 OID 25179)
-- Name: doctor fk_doctor_contact1; Type: FK CONSTRAINT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.doctor
    ADD CONSTRAINT fk_doctor_contact1 FOREIGN KEY (contact_id) REFERENCES hospital.contact(contact_id);


--
-- TOC entry 3374 (class 2606 OID 25292)
-- Name: hospital fk_hospital_address1; Type: FK CONSTRAINT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.hospital
    ADD CONSTRAINT fk_hospital_address1 FOREIGN KEY (address_id) REFERENCES hospital.address(address_id);


--
-- TOC entry 3375 (class 2606 OID 25306)
-- Name: hospital_has_patient fk_hospital_has_patient_hospital1; Type: FK CONSTRAINT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.hospital_has_patient
    ADD CONSTRAINT fk_hospital_has_patient_hospital1 FOREIGN KEY (hospital_id) REFERENCES hospital.hospital(hospital_id);


--
-- TOC entry 3376 (class 2606 OID 26123)
-- Name: hospital_has_patient fk_hospital_has_patient_patient1; Type: FK CONSTRAINT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.hospital_has_patient
    ADD CONSTRAINT fk_hospital_has_patient_patient1 FOREIGN KEY (patient_id) REFERENCES hospital.patient(patient_id) ON DELETE CASCADE;


--
-- TOC entry 3363 (class 2606 OID 25158)
-- Name: patient fk_patient_contact1; Type: FK CONSTRAINT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.patient
    ADD CONSTRAINT fk_patient_contact1 FOREIGN KEY (contact_id) REFERENCES hospital.contact(contact_id);


--
-- TOC entry 3372 (class 2606 OID 25276)
-- Name: patient_has_address fk_patient_has_address_address1; Type: FK CONSTRAINT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.patient_has_address
    ADD CONSTRAINT fk_patient_has_address_address1 FOREIGN KEY (address_id) REFERENCES hospital.address(address_id);


--
-- TOC entry 3373 (class 2606 OID 26118)
-- Name: patient_has_address fk_patient_has_address_patient1; Type: FK CONSTRAINT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.patient_has_address
    ADD CONSTRAINT fk_patient_has_address_patient1 FOREIGN KEY (patient_id) REFERENCES hospital.patient(patient_id) ON DELETE CASCADE;


--
-- TOC entry 3370 (class 2606 OID 25250)
-- Name: patient_in_department fk_patient_has_department_department1; Type: FK CONSTRAINT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.patient_in_department
    ADD CONSTRAINT fk_patient_has_department_department1 FOREIGN KEY (department_id) REFERENCES hospital.department(department_id);


--
-- TOC entry 3371 (class 2606 OID 26113)
-- Name: patient_in_department fk_patient_has_department_patient1; Type: FK CONSTRAINT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.patient_in_department
    ADD CONSTRAINT fk_patient_has_department_patient1 FOREIGN KEY (patient_id) REFERENCES hospital.patient(patient_id) ON DELETE CASCADE;


--
-- TOC entry 3368 (class 2606 OID 25224)
-- Name: patient_has_diagnose fk_patient_has_diagnose_diagnose1; Type: FK CONSTRAINT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.patient_has_diagnose
    ADD CONSTRAINT fk_patient_has_diagnose_diagnose1 FOREIGN KEY (diagnose_id) REFERENCES hospital.diagnose(diagnose_id);


--
-- TOC entry 3369 (class 2606 OID 26108)
-- Name: patient_has_diagnose fk_patient_has_diagnose_patient1; Type: FK CONSTRAINT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.patient_has_diagnose
    ADD CONSTRAINT fk_patient_has_diagnose_patient1 FOREIGN KEY (patient_id) REFERENCES hospital.patient(patient_id) ON DELETE CASCADE;


--
-- TOC entry 3366 (class 2606 OID 25198)
-- Name: patient_has_doctor fk_patient_has_doctor_doctor1; Type: FK CONSTRAINT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.patient_has_doctor
    ADD CONSTRAINT fk_patient_has_doctor_doctor1 FOREIGN KEY (doctor_id) REFERENCES hospital.doctor(doctor_id);


--
-- TOC entry 3367 (class 2606 OID 26128)
-- Name: patient_has_doctor fk_patient_has_doctor_patient1; Type: FK CONSTRAINT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.patient_has_doctor
    ADD CONSTRAINT fk_patient_has_doctor_patient1 FOREIGN KEY (patient_id) REFERENCES hospital.patient(patient_id) ON DELETE CASCADE;


--
-- TOC entry 3364 (class 2606 OID 25163)
-- Name: patient fk_patient_insurance_company1; Type: FK CONSTRAINT; Schema: hospital; Owner: postgres
--

ALTER TABLE ONLY hospital.patient
    ADD CONSTRAINT fk_patient_insurance_company1 FOREIGN KEY (insurance_company_id) REFERENCES hospital.insurance_company(insurance_company_id);


--
-- TOC entry 3564 (class 0 OID 0)
-- Dependencies: 5
-- Name: SCHEMA hospital; Type: ACL; Schema: -; Owner: postgres
--

GRANT ALL ON SCHEMA hospital TO java;


--
-- TOC entry 3567 (class 0 OID 0)
-- Dependencies: 235
-- Name: TABLE address; Type: ACL; Schema: hospital; Owner: postgres
--

GRANT ALL ON TABLE hospital.address TO java;


--
-- TOC entry 3569 (class 0 OID 0)
-- Dependencies: 234
-- Name: SEQUENCE address_address_id_seq; Type: ACL; Schema: hospital; Owner: postgres
--

GRANT ALL ON SEQUENCE hospital.address_address_id_seq TO java;


--
-- TOC entry 3570 (class 0 OID 0)
-- Dependencies: 212
-- Name: TABLE contact; Type: ACL; Schema: hospital; Owner: postgres
--

GRANT ALL ON TABLE hospital.contact TO java;


--
-- TOC entry 3572 (class 0 OID 0)
-- Dependencies: 211
-- Name: SEQUENCE contact_contact_id_seq; Type: ACL; Schema: hospital; Owner: postgres
--

GRANT ALL ON SEQUENCE hospital.contact_contact_id_seq TO java;


--
-- TOC entry 3573 (class 0 OID 0)
-- Dependencies: 230
-- Name: TABLE department; Type: ACL; Schema: hospital; Owner: postgres
--

GRANT SELECT ON TABLE hospital.department TO student;
GRANT ALL ON TABLE hospital.department TO java;


--
-- TOC entry 3575 (class 0 OID 0)
-- Dependencies: 229
-- Name: SEQUENCE department_department_id_seq; Type: ACL; Schema: hospital; Owner: postgres
--

GRANT ALL ON SEQUENCE hospital.department_department_id_seq TO java;


--
-- TOC entry 3576 (class 0 OID 0)
-- Dependencies: 225
-- Name: TABLE diagnose; Type: ACL; Schema: hospital; Owner: postgres
--

GRANT ALL ON TABLE hospital.diagnose TO java;


--
-- TOC entry 3578 (class 0 OID 0)
-- Dependencies: 224
-- Name: SEQUENCE diagnose_diagnose_id_seq; Type: ACL; Schema: hospital; Owner: postgres
--

GRANT ALL ON SEQUENCE hospital.diagnose_diagnose_id_seq TO java;


--
-- TOC entry 3579 (class 0 OID 0)
-- Dependencies: 220
-- Name: TABLE doctor; Type: ACL; Schema: hospital; Owner: postgres
--

GRANT ALL ON TABLE hospital.doctor TO java;


--
-- TOC entry 3581 (class 0 OID 0)
-- Dependencies: 219
-- Name: SEQUENCE doctor_contact_id_seq; Type: ACL; Schema: hospital; Owner: postgres
--

GRANT ALL ON SEQUENCE hospital.doctor_contact_id_seq TO java;


--
-- TOC entry 3583 (class 0 OID 0)
-- Dependencies: 218
-- Name: SEQUENCE doctor_doctor_id_seq; Type: ACL; Schema: hospital; Owner: postgres
--

GRANT ALL ON SEQUENCE hospital.doctor_doctor_id_seq TO java;


--
-- TOC entry 3584 (class 0 OID 0)
-- Dependencies: 251
-- Name: TABLE dummy1; Type: ACL; Schema: hospital; Owner: postgres
--

GRANT ALL ON TABLE hospital.dummy1 TO java;


--
-- TOC entry 3585 (class 0 OID 0)
-- Dependencies: 250
-- Name: TABLE dummy2; Type: ACL; Schema: hospital; Owner: postgres
--

GRANT ALL ON TABLE hospital.dummy2 TO java;


--
-- TOC entry 3586 (class 0 OID 0)
-- Dependencies: 241
-- Name: TABLE hospital; Type: ACL; Schema: hospital; Owner: postgres
--

GRANT ALL ON TABLE hospital.hospital TO java;


--
-- TOC entry 3588 (class 0 OID 0)
-- Dependencies: 240
-- Name: SEQUENCE hospital_address_id_seq; Type: ACL; Schema: hospital; Owner: postgres
--

GRANT ALL ON SEQUENCE hospital.hospital_address_id_seq TO java;


--
-- TOC entry 3589 (class 0 OID 0)
-- Dependencies: 244
-- Name: TABLE hospital_has_patient; Type: ACL; Schema: hospital; Owner: postgres
--

GRANT ALL ON TABLE hospital.hospital_has_patient TO java;


--
-- TOC entry 3591 (class 0 OID 0)
-- Dependencies: 242
-- Name: SEQUENCE hospital_has_patient_hospital_id_seq; Type: ACL; Schema: hospital; Owner: postgres
--

GRANT ALL ON SEQUENCE hospital.hospital_has_patient_hospital_id_seq TO java;


--
-- TOC entry 3593 (class 0 OID 0)
-- Dependencies: 243
-- Name: SEQUENCE hospital_has_patient_patient_id_seq; Type: ACL; Schema: hospital; Owner: postgres
--

GRANT ALL ON SEQUENCE hospital.hospital_has_patient_patient_id_seq TO java;


--
-- TOC entry 3595 (class 0 OID 0)
-- Dependencies: 239
-- Name: SEQUENCE hospital_hospital_id_seq; Type: ACL; Schema: hospital; Owner: postgres
--

GRANT ALL ON SEQUENCE hospital.hospital_hospital_id_seq TO java;


--
-- TOC entry 3596 (class 0 OID 0)
-- Dependencies: 214
-- Name: TABLE insurance_company; Type: ACL; Schema: hospital; Owner: postgres
--

GRANT SELECT ON TABLE hospital.insurance_company TO student;
GRANT ALL ON TABLE hospital.insurance_company TO java;


--
-- TOC entry 3598 (class 0 OID 0)
-- Dependencies: 213
-- Name: SEQUENCE insurance_company_insurance_company_id_seq; Type: ACL; Schema: hospital; Owner: postgres
--

GRANT ALL ON SEQUENCE hospital.insurance_company_insurance_company_id_seq TO java;


--
-- TOC entry 3599 (class 0 OID 0)
-- Dependencies: 217
-- Name: TABLE patient; Type: ACL; Schema: hospital; Owner: postgres
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE hospital.patient TO teacher;
GRANT ALL ON TABLE hospital.patient TO java;


--
-- TOC entry 3600 (class 0 OID 0)
-- Dependencies: 233
-- Name: TABLE patient_in_department; Type: ACL; Schema: hospital; Owner: postgres
--

GRANT ALL ON TABLE hospital.patient_in_department TO java;


--
-- TOC entry 3601 (class 0 OID 0)
-- Dependencies: 247
-- Name: TABLE mat_number_of_pacient_in_interna; Type: ACL; Schema: hospital; Owner: postgres
--

GRANT ALL ON TABLE hospital.mat_number_of_pacient_in_interna TO java;


--
-- TOC entry 3602 (class 0 OID 0)
-- Dependencies: 223
-- Name: TABLE patient_has_doctor; Type: ACL; Schema: hospital; Owner: postgres
--

GRANT ALL ON TABLE hospital.patient_has_doctor TO java;


--
-- TOC entry 3603 (class 0 OID 0)
-- Dependencies: 245
-- Name: TABLE pat_doc_depart; Type: ACL; Schema: hospital; Owner: postgres
--

GRANT ALL ON TABLE hospital.pat_doc_depart TO java;


--
-- TOC entry 3605 (class 0 OID 0)
-- Dependencies: 216
-- Name: SEQUENCE patient_contact_id_seq; Type: ACL; Schema: hospital; Owner: postgres
--

GRANT ALL ON SEQUENCE hospital.patient_contact_id_seq TO java;


--
-- TOC entry 3606 (class 0 OID 0)
-- Dependencies: 238
-- Name: TABLE patient_has_address; Type: ACL; Schema: hospital; Owner: postgres
--

GRANT ALL ON TABLE hospital.patient_has_address TO java;


--
-- TOC entry 3608 (class 0 OID 0)
-- Dependencies: 237
-- Name: SEQUENCE patient_has_address_address_id_seq; Type: ACL; Schema: hospital; Owner: postgres
--

GRANT ALL ON SEQUENCE hospital.patient_has_address_address_id_seq TO java;


--
-- TOC entry 3610 (class 0 OID 0)
-- Dependencies: 236
-- Name: SEQUENCE patient_has_address_patient_id_seq; Type: ACL; Schema: hospital; Owner: postgres
--

GRANT ALL ON SEQUENCE hospital.patient_has_address_patient_id_seq TO java;


--
-- TOC entry 3611 (class 0 OID 0)
-- Dependencies: 228
-- Name: TABLE patient_has_diagnose; Type: ACL; Schema: hospital; Owner: postgres
--

GRANT ALL ON TABLE hospital.patient_has_diagnose TO java;


--
-- TOC entry 3613 (class 0 OID 0)
-- Dependencies: 227
-- Name: SEQUENCE patient_has_diagnose_diagnose_id_seq; Type: ACL; Schema: hospital; Owner: postgres
--

GRANT ALL ON SEQUENCE hospital.patient_has_diagnose_diagnose_id_seq TO java;


--
-- TOC entry 3615 (class 0 OID 0)
-- Dependencies: 226
-- Name: SEQUENCE patient_has_diagnose_patient_id_seq; Type: ACL; Schema: hospital; Owner: postgres
--

GRANT ALL ON SEQUENCE hospital.patient_has_diagnose_patient_id_seq TO java;


--
-- TOC entry 3617 (class 0 OID 0)
-- Dependencies: 222
-- Name: SEQUENCE patient_has_doctor_doctor_id_seq; Type: ACL; Schema: hospital; Owner: postgres
--

GRANT ALL ON SEQUENCE hospital.patient_has_doctor_doctor_id_seq TO java;


--
-- TOC entry 3619 (class 0 OID 0)
-- Dependencies: 221
-- Name: SEQUENCE patient_has_doctor_patient_id_seq; Type: ACL; Schema: hospital; Owner: postgres
--

GRANT ALL ON SEQUENCE hospital.patient_has_doctor_patient_id_seq TO java;


--
-- TOC entry 3621 (class 0 OID 0)
-- Dependencies: 232
-- Name: SEQUENCE patient_in_department_department_id_seq; Type: ACL; Schema: hospital; Owner: postgres
--

GRANT ALL ON SEQUENCE hospital.patient_in_department_department_id_seq TO java;


--
-- TOC entry 3623 (class 0 OID 0)
-- Dependencies: 231
-- Name: SEQUENCE patient_in_department_patient_id_seq; Type: ACL; Schema: hospital; Owner: postgres
--

GRANT ALL ON SEQUENCE hospital.patient_in_department_patient_id_seq TO java;


--
-- TOC entry 3625 (class 0 OID 0)
-- Dependencies: 215
-- Name: SEQUENCE patient_patient_id_seq; Type: ACL; Schema: hospital; Owner: postgres
--

GRANT ALL ON SEQUENCE hospital.patient_patient_id_seq TO java;


--
-- TOC entry 3626 (class 0 OID 0)
-- Dependencies: 246
-- Name: TABLE teachers_view_on_user; Type: ACL; Schema: hospital; Owner: postgres
--

GRANT SELECT ON TABLE hospital.teachers_view_on_user TO teacher;
GRANT ALL ON TABLE hospital.teachers_view_on_user TO java;


--
-- TOC entry 3627 (class 0 OID 0)
-- Dependencies: 249
-- Name: TABLE "user"; Type: ACL; Schema: hospital; Owner: postgres
--

GRANT ALL ON TABLE hospital."user" TO java;


--
-- TOC entry 3629 (class 0 OID 0)
-- Dependencies: 248
-- Name: SEQUENCE user_user_id_seq; Type: ACL; Schema: hospital; Owner: postgres
--

GRANT ALL ON SEQUENCE hospital.user_user_id_seq TO java;


--
-- TOC entry 3554 (class 0 OID 25982)
-- Dependencies: 247 3560
-- Name: mat_number_of_pacient_in_interna; Type: MATERIALIZED VIEW DATA; Schema: hospital; Owner: postgres
--

REFRESH MATERIALIZED VIEW hospital.mat_number_of_pacient_in_interna;


-- Completed on 2021-12-31 17:49:06

--
-- PostgreSQL database dump complete
--

