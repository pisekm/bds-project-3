package org.but.feec.hospital.controller;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.but.feec.hospital.App;
import org.but.feec.hospital.Main;
import org.but.feec.hospital.api.PersonCreateView;
import org.but.feec.hospital.api.PersonDetailView;
import org.but.feec.hospital.api.PersonView;
import org.but.feec.hospital.data.PersonRepository;
import org.but.feec.hospital.service.PersonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

public class PersonsController {

    private static final Logger logger = LoggerFactory.getLogger(PersonsController.class);

    @FXML
    private TableColumn<PersonView, String> PersonsPersonalNo;

    @FXML
    private Button addUserButton;

    @FXML
    private TableColumn<PersonView, String> personsDateOfBirth;

    @FXML
    private TableColumn<PersonView, Long> personsId;

    @FXML
    private TableColumn<PersonView, String> personsName;

    @FXML
    private TableColumn<PersonView, String> personsSurname;

    @FXML
    private TableView<PersonView> personsTableView;

    @FXML
    private Button refreshButton;

    @FXML
    private Button closeButton;

    @FXML
    private Button okButton;

    @FXML
    private Button findPersonButton;

    @FXML
    public MenuItem closeMenu;

    @FXML
    public MenuItem logoutMenu;

    @FXML
    private TextField findBySurname;

    @FXML
    private TextField findBySurnameInjection;

    private PersonService personService;
    private PersonRepository personRepository;

    public PersonsController() {
    }

    @FXML
    private void initialize() {
        personRepository = new PersonRepository();
        personService = new PersonService(personRepository);

        personsId.setCellValueFactory(new PropertyValueFactory<PersonView, Long>("id"));
        personsName.setCellValueFactory(new PropertyValueFactory<PersonView, String>("name"));
        personsSurname.setCellValueFactory(new PropertyValueFactory<PersonView, String>("surname"));
        personsDateOfBirth.setCellValueFactory(new PropertyValueFactory<PersonView, String>("dateOfBirth"));
        PersonsPersonalNo.setCellValueFactory(new PropertyValueFactory<PersonView, String>("personalNo"));

        ObservableList<PersonView> observablePersonsList = initializePersonsData();
        personsTableView.setItems(observablePersonsList);

        personsTableView.getSortOrder().add(personsId);

        initializeTableViewSelection();

        logger.info("PersonsController initialized");
    }

    private void initializeTableViewSelection() {
        MenuItem detailedView = new MenuItem("Detailed patient view");
        detailedView.setOnAction((ActionEvent event) -> {
            PersonView personView = personsTableView.getSelectionModel().getSelectedItem();
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(App.class.getResource("fxml/PersonsDetailView.fxml"));
            Stage stage = new Stage();

            Long personId = personView.getId();
            PersonDetailView personDetailView = personService.getPersonDetailView(personId);

            stage.setUserData(personDetailView);
            stage.setTitle("Patients detailed view");
            stage.getIcons().add(new Image(App.class.getResourceAsStream("logos/znak.jpg")));

            PersonsDetailViewController controller = new PersonsDetailViewController();
            controller.setStage(stage);
            fxmlLoader.setController(controller);

            try {
                Scene scene = new Scene(fxmlLoader.load(), 398, 597);
                stage.setTitle("Detailed patient view");
                stage.setScene(scene);
                stage.show();
            }
            catch (IOException e) {
                e.printStackTrace();
                logger.error("Could not load detailed person view.", e.getMessage());
            }
        });

        MenuItem edit = new MenuItem("Edit/Delete patient");
        edit.setOnAction((ActionEvent event) -> {
            PersonView personView = personsTableView.getSelectionModel().getSelectedItem();
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(App.class.getResource("fxml/PersonEdit.fxml"));

            Stage stage = new Stage();
            Long personId = personView.getId();
            PersonDetailView personDetailView = personService.getPersonDetailView(personId);

            stage.setUserData(personDetailView);
            stage.setTitle("Edit patient");
            stage.getIcons().add(new Image(App.class.getResourceAsStream("logos/znak.jpg")));

            PersonsEditController controller = new PersonsEditController();
            controller.setStage(stage);
            fxmlLoader.setController(controller);

            try {
                Scene scene = new Scene(fxmlLoader.load(), 528, 387);
                stage.setTitle("Edit patient");
                stage.setScene(scene);
                stage.show();
            }
            catch (IOException e) {
                e.printStackTrace();
                logger.error("Could not load edit person.", e.getMessage());
            }
        });

        ContextMenu menu = new ContextMenu();
        menu.getItems().addAll(detailedView);
        menu.getItems().add(edit);
        personsTableView.setContextMenu(menu);
    }

    private ObservableList<PersonView> initializePersonsData() {
        List<PersonView> persons = personService.getPersonsView();
        return FXCollections.observableArrayList(persons);
    }

    public void handleAddUserButton(ActionEvent event) {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(App.class.getResource("fxml/PersonsCreate.fxml"));
        try {
            Scene scene = new Scene(fxmlLoader.load(), 499, 352);
            Stage stage = new Stage();
            stage.setTitle("Create user");
            stage.setScene(scene);
            stage.getIcons().add(new Image(App.class.getResourceAsStream("logos/znak.jpg")));
            stage.show();
        }
        catch (IOException e) {
            e.printStackTrace();
            logger.error("Could not load create user.", e.getMessage());
        }
    }

    public void handleRefreshButton(ActionEvent actionEvent) {
        ObservableList<PersonView> observablePersonsList = initializePersonsData();
        personsTableView.setItems(observablePersonsList);
        personsTableView.refresh();
        personsTableView.getSortOrder().add(personsId);

        findBySurname.clear();
        findBySurnameInjection.clear();
    }

    public void handleCloseMenu(ActionEvent actionEvent) {
        System.exit(0);
    }

    public void handleLogoutMenu(ActionEvent actionEvent){
        Stage stageOld = (Stage) closeButton.getScene().getWindow();
        stageOld.close();

        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(App.class.getResource("Login.fxml"));
            VBox mainStage = fxmlLoader.load();

            Stage primaryStage = new Stage();
            primaryStage.setTitle("Patient repository");
            primaryStage.getIcons().add(new Image(App.class.getResourceAsStream("logos/znak.jpg")));
            Scene scene = new Scene(mainStage);

            primaryStage.setScene(scene);
            primaryStage.show();
        }
        catch (IOException e){
            e.printStackTrace();
            logger.error("Could not load login.", e.getMessage());
        }
    }

    public void handleSqlInjection(ActionEvent actionEvent) {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(App.class.getResource("fxml/SqlInjection.fxml"));

        try {
            Scene scene = new Scene(fxmlLoader.load(), 499, 352);
            Stage stage = new Stage();
            stage.setTitle("SQL injection - add role");
            stage.setScene(scene);
            stage.getIcons().add(new Image(App.class.getResourceAsStream("logos/znak.jpg")));
            stage.show();
        }
        catch (IOException e) {
            e.printStackTrace();
            logger.error("Could not load SQL injection.", e.getMessage());
        }
    }

    public void handleFindPersonButton(ActionEvent actionEvent) {
        String surname = findBySurname.getText();

        ObservableList<PersonView> observablePersonsList = initializePersonsDataFiltered(surname);

        personsTableView.setItems(observablePersonsList);
        personsTableView.getSortOrder().add(personsId);

        initializeTableViewSelection();
    }

    private ObservableList<PersonView> initializePersonsDataFiltered(String surname) {
        List<PersonView> filteredPersons = personService.getPersonsViewBySurname(surname);
        return FXCollections.observableArrayList(filteredPersons);
    }

    public void handleFindPersonButtonInjection(ActionEvent actionEvent) {
        String surname = findBySurnameInjection.getText();

        ObservableList<PersonView> observablePersonsList = initializePersonsDataFilteredInjection(surname);

        personsTableView.setItems(observablePersonsList);
        personsTableView.getSortOrder().add(personsId);

        initializeTableViewSelection();
    }

    private ObservableList<PersonView> initializePersonsDataFilteredInjection(String surname) {
        List<PersonView> filteredPersons = personService.getPersonsViewBySurnameStatement(surname);
        return FXCollections.observableArrayList(filteredPersons);
    }
}

