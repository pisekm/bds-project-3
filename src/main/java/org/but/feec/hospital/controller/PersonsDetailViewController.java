package org.but.feec.hospital.controller;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.but.feec.hospital.api.PersonDetailView;
import org.but.feec.hospital.api.PersonView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PersonsDetailViewController {

    private static final Logger logger = LoggerFactory.getLogger(PersonsDetailViewController.class);

    @FXML
    private TextField cityTextField;

    @FXML
    private TextField dateOfBirthTextField;

    @FXML
    private TextField departmentTextField;

    @FXML
    private TextField diagnoseTextField;

    @FXML
    private TextField floorTextField;

    @FXML
    private TextField hospitalNameTextField;

    @FXML
    private TextField idTextField;

    @FXML
    private TextField insuranceCompanyTextField;

    @FXML
    private TextField nameTextField;

    @FXML
    private TextField personalNoTextField;

    @FXML
    private TextField roomTextField;

    @FXML
    private TextField surnameTextField;

    @FXML
    private Button closeButton;

    public Stage stage;

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    public void initialize() {
        idTextField.setEditable(false);
        nameTextField.setEditable(false);
        surnameTextField.setEditable(false);
        dateOfBirthTextField.setEditable(false);
        personalNoTextField.setEditable(false);
        cityTextField.setEditable(false);
        hospitalNameTextField.setEditable(false);
        departmentTextField.setEditable(false);
        floorTextField.setEditable(false);
        roomTextField.setEditable(false);
        diagnoseTextField.setEditable(false);

        loadPersonsData();

        logger.info("PersonsDetailViewController initialized");
    }

    private void loadPersonsData() {
        Stage stage = this.stage;
        if (stage.getUserData() instanceof PersonDetailView) {
            PersonDetailView personBasicView = (PersonDetailView) stage.getUserData();
            idTextField.setText(String.valueOf(personBasicView.getId()));
            nameTextField.setText(personBasicView.getName());
            surnameTextField.setText(personBasicView.getSurname());
            dateOfBirthTextField.setText(personBasicView.getDateOfBirth());
            personalNoTextField.setText(personBasicView.getPersonalNo());
            insuranceCompanyTextField.setText(personBasicView.getInsuranceCompany());
            cityTextField.setText(personBasicView.getCity());
            hospitalNameTextField.setText(personBasicView.getHospitalName());
            departmentTextField.setText(personBasicView.getDepartment());
            floorTextField.setText(String.valueOf(personBasicView.getFloor()));
            roomTextField.setText(String.valueOf(personBasicView.getRoom()));
            diagnoseTextField.setText(personBasicView.getDiagnose());
        }
    }

    @FXML
    public void handleCloseButton(ActionEvent event) {
        Stage stage = (Stage) closeButton.getScene().getWindow();
        stage.close();
    }
}

