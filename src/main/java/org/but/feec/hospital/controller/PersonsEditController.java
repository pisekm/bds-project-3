package org.but.feec.hospital.controller;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.but.feec.hospital.App;
import org.but.feec.hospital.api.PersonDetailView;
import org.but.feec.hospital.api.PersonEditView;
import org.but.feec.hospital.data.PersonRepository;
import org.but.feec.hospital.service.PersonService;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

public class PersonsEditController {

    private static final Logger logger = LoggerFactory.getLogger(PersonsEditController.class);

    @FXML
    private TextField cityTextField;

    @FXML
    private Button editButton;

    @FXML
    private TextField houseNumberTextField;

    @FXML
    private TextField idTextField;

    @FXML
    private TextField nameTextField;

    @FXML
    private TextField streetTextField;

    @FXML
    private TextField surnameTextField;

    @FXML
    private TextField zipCodeTextField;

    private PersonService personService;
    private PersonRepository personRepository;
    private ValidationSupport validation;
    public Stage stage;

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    public void handleEditButton(ActionEvent event) {

        PersonEditView personEditView = new PersonEditView();
        personEditView.setId(Long.valueOf(idTextField.getText()));
        personEditView.setName(nameTextField.getText());
        personEditView.setSurname(surnameTextField.getText());
        personEditView.setCity(cityTextField.getText());
        personEditView.setStreet(streetTextField.getText());
        personEditView.setHouseNumber(houseNumberTextField.getText());
        personEditView.setZipCode(Long.valueOf(zipCodeTextField.getText()));

        personService.editPerson(personEditView);

        personEditedConfirmationDialog();
    }

    @FXML
    public void handleDeleteButton(ActionEvent event) {

        PersonEditView personEditView = new PersonEditView();
        personEditView.setId(Long.valueOf(idTextField.getText()));

        personService.deletePerson(personEditView);

        personDeletedConfirmationDialog();
    }

    private void personDeletedConfirmationDialog() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Patient Deleted Confirmation");
        alert.setHeaderText("Your patient was successfully deleted.");

        Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image(App.class.getResourceAsStream("logos/znak.jpg")));

        Timeline idlestage = new Timeline(new KeyFrame(Duration.seconds(5), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                alert.setResult(ButtonType.CANCEL);
                alert.hide();
            }
        }));
        idlestage.setCycleCount(1);
        idlestage.play();
        Optional<ButtonType> result = alert.showAndWait();

        Stage stageOld = (Stage) editButton.getScene().getWindow();
        stageOld.close();
    }

    private void personEditedConfirmationDialog() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Patient Edited Confirmation");
        alert.setHeaderText("Your patient was successfully edited.");

        Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image(App.class.getResourceAsStream("logos/znak.jpg")));

        Timeline idlestage = new Timeline(new KeyFrame(Duration.seconds(5), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                alert.setResult(ButtonType.CANCEL);
                alert.hide();
            }
        }));
        idlestage.setCycleCount(1);
        idlestage.play();
        Optional<ButtonType> result = alert.showAndWait();

        Stage stageOld = (Stage) editButton.getScene().getWindow();
        stageOld.close();
    }

    @FXML
    public void initialize() {
        personRepository = new PersonRepository();
        personService = new PersonService(personRepository);

        idTextField.setEditable(false);

        validation = new ValidationSupport();
        validation.registerValidator(nameTextField, Validator.createEmptyValidator("The name must not be empty."));
        validation.registerValidator(surnameTextField, Validator.createEmptyValidator("The surname must not be empty."));
        validation.registerValidator(cityTextField, Validator.createEmptyValidator("The city must not be empty."));
        validation.registerValidator(streetTextField, Validator.createEmptyValidator("The street must not be empty."));
        validation.registerValidator(houseNumberTextField, Validator.createEmptyValidator("The house number must not be empty."));
        validation.registerValidator(zipCodeTextField, Validator.createEmptyValidator("The ZIP code must not be empty."));

        editButton.disableProperty().bind(validation.invalidProperty());

        loadPersonsData();

        logger.info("PersonsEditController initialized");
    }

    private void loadPersonsData() {
        Stage stage = this.stage;
        if (stage.getUserData() instanceof PersonDetailView) {
            PersonDetailView personDetailView = (PersonDetailView) stage.getUserData();
            idTextField.setText(String.valueOf(personDetailView.getId()));
            nameTextField.setText(personDetailView.getName());
            surnameTextField.setText(personDetailView.getSurname());
            cityTextField.setText(personDetailView.getCity());
            streetTextField.setText(personDetailView.getStreet());
            houseNumberTextField.setText(personDetailView.getHouseNumber());
            zipCodeTextField.setText(String.valueOf(personDetailView.getZipCode()));
        }
    }

}

