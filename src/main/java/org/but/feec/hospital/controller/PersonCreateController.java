package org.but.feec.hospital.controller;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.but.feec.hospital.App;
import org.but.feec.hospital.api.PersonCreateView;
import org.but.feec.hospital.api.PersonView;
import org.but.feec.hospital.data.PersonRepository;
import org.but.feec.hospital.service.PersonService;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

public class PersonCreateController {

    private static final Logger logger = LoggerFactory.getLogger(PersonCreateController.class);

    @FXML
    private TextField newName;

    @FXML
    private TextField newNickname;

    @FXML
    private PasswordField newPassword;

    @FXML
    private TextField newSurname;

    @FXML
    private Button submitButton;

    @FXML
    private Button submitInjectionButton;

    @FXML
    private Button findPersonButtonInjection;

    @FXML
    private TextField findBySurnameInjection;

    private ValidationSupport validation;
    private PersonService personService;
    private PersonRepository personRepository;
    private PersonsController personsController;

    @FXML
    public void initialize() {
        //personsController = new PersonsController();
        personRepository = new PersonRepository();
        personService = new PersonService(personRepository);

        validation = new ValidationSupport();
        validation.registerValidator(newName, Validator.createEmptyValidator("The name must not be empty."));
        validation.registerValidator(newNickname, Validator.createEmptyValidator("The nickname must not be empty."));
        validation.registerValidator(newSurname, Validator.createEmptyValidator("The surname must not be empty."));
        validation.registerValidator(newPassword, Validator.createEmptyValidator("The password must not be empty."));

        submitButton.disableProperty().bind(validation.invalidProperty());

        logger.info("PersonCreateController initialized");
    }

    @FXML
    void handleSubmitNewPerson(ActionEvent event) {
        String name = newName.getText();
        String nickname = newNickname.getText();
        String surname = newSurname.getText();
        String password = newPassword.getText();

        PersonCreateView personCreateView = new PersonCreateView();

        personCreateView.setName(name);
        personCreateView.setNickname(nickname);
        personCreateView.setSurname(surname);
        personCreateView.setPassword(password.toCharArray());

        personService.createPerson(personCreateView);

        personCreatedConfirmationDialog();
    }

    private void personCreatedConfirmationDialog() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("User Created Confirmation");
        alert.setHeaderText("Your new user was successfully created.");

        Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image(App.class.getResourceAsStream("logos/znak.jpg")));

        Timeline idlestage = new Timeline(new KeyFrame(Duration.seconds(5), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                alert.setResult(ButtonType.CANCEL);
                alert.hide();
            }
        }));
        idlestage.setCycleCount(1);
        idlestage.play();
        Optional<ButtonType> result = alert.showAndWait();

        Stage stageOld = (Stage) submitButton.getScene().getWindow();
        stageOld.close();
    }
    @FXML
    public void handleSubmitNewPersonInjection(ActionEvent actionEvent) {
        String name = newName.getText();
        String nickname = newNickname.getText();
        String surname = newSurname.getText();
        String password = newPassword.getText();

        PersonCreateView personCreateView = new PersonCreateView();

        personCreateView.setName(name);
        personCreateView.setNickname(nickname);
        personCreateView.setSurname(surname);
        personCreateView.setPassword(password.toCharArray());

        personService.createPersonStatement(personCreateView);

        personCreatedConfirmationDialog();
    }
}

