package org.but.feec.hospital.api;

import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PersonDetailView {
    private LongProperty id = new SimpleLongProperty();
    private StringProperty name = new SimpleStringProperty();
    private StringProperty surname = new SimpleStringProperty();
    private StringProperty dateOfBirth = new SimpleStringProperty();
    private StringProperty personalNo = new SimpleStringProperty();

    private StringProperty insuranceCompany = new SimpleStringProperty();
    private StringProperty city = new SimpleStringProperty();
    private StringProperty hospitalName = new SimpleStringProperty();
    private StringProperty department = new SimpleStringProperty();
    private LongProperty floor = new SimpleLongProperty();
    private LongProperty room = new SimpleLongProperty();
    private StringProperty diagnose = new SimpleStringProperty();

    private StringProperty phoneNumber = new SimpleStringProperty();
    private StringProperty street = new SimpleStringProperty();
    private StringProperty houseNumber = new SimpleStringProperty();
    private LongProperty zipCode = new SimpleLongProperty();

    public long getId() {
        return idProperty().get();
    }

    public LongProperty idProperty() {
        return id;
    }

    public void setId(long id) {
        this.idProperty().setValue(id);
    }

    public String getName() {
        return nameProperty().get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.nameProperty().setValue(name);
    }

    public String getSurname() {
        return surnameProperty().get();
    }

    public StringProperty surnameProperty() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surnameProperty().setValue(surname);
    }

    public String getDateOfBirth() {
        return dateOfBirthProperty().get();
    }

    public StringProperty dateOfBirthProperty() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirthProperty().setValue(dateOfBirth);
    }

    public String getPersonalNo() {
        return personalNoProperty().get();
    }

    public StringProperty personalNoProperty() {
        return personalNo;
    }

    public void setPersonalNo(String personalNo) {
        this.personalNoProperty().setValue(personalNo);
    }

    public String getInsuranceCompany() {
        return insuranceCompanyProperty().get();
    }

    public StringProperty insuranceCompanyProperty() {
        return insuranceCompany;
    }

    public void setInsuranceCompany(String insuranceCompany) {
        this.insuranceCompanyProperty().setValue(insuranceCompany);
    }

    public String getCity() {
        return cityProperty().get();
    }

    public StringProperty cityProperty() {
        return city;
    }

    public void setCity(String city) {
        this.cityProperty().setValue(city);
    }

    public String getHospitalName() {
        return hospitalNameProperty().get();
    }

    public StringProperty hospitalNameProperty() {
        return hospitalName;
    }

    public void setHospitalName(String hospitalName) {
        this.hospitalNameProperty().setValue(hospitalName);
    }

    public String getDepartment() {
        return departmentProperty().get();
    }

    public StringProperty departmentProperty() {
        return department;
    }

    public void setDepartment(String department) {
        this.departmentProperty().setValue(department);
    }

    public long getFloor() {
        return floorProperty().get();
    }

    public LongProperty floorProperty() {
        return floor;
    }

    public void setFloor(long floor) {
        this.floorProperty().setValue(floor);
    }

    public long getRoom() {
        return roomProperty().get();
    }

    public LongProperty roomProperty() {
        return room;
    }

    public void setRoom(long room) {
        this.roomProperty().setValue(room);
    }

    public String getDiagnose() {
        return diagnoseProperty().get();
    }

    public StringProperty diagnoseProperty() {
        return diagnose;
    }

    public void setDiagnose(String diagnose) {
        this.diagnoseProperty().setValue(diagnose);
    }

    public String getPhoneNumber() {
        return phoneNumberProperty().get();
    }

    public StringProperty phoneNumberProperty() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumberProperty().setValue(phoneNumber);
    }

    public String getStreet() {
        return streetProperty().get();
    }

    public StringProperty streetProperty() {
        return street;
    }

    public void setStreet(String street) {
        this.streetProperty().setValue(street);
    }

    public String getHouseNumber() {
        return houseNumberProperty().get();
    }

    public StringProperty houseNumberProperty() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumberProperty().setValue(houseNumber);
    }

    public long getZipCode() {
        return zipCodeProperty().get();
    }

    public LongProperty zipCodeProperty() {
        return zipCode;
    }

    public void setZipCode(long zipCode) {
        this.zipCodeProperty().setValue(zipCode);
    }
}
