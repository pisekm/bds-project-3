package org.but.feec.hospital.data;

import org.but.feec.hospital.api.*;
import org.but.feec.hospital.config.DataSourceConfig;
import org.but.feec.hospital.controller.LoginController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PersonRepository {

    private static final Logger logger = LoggerFactory.getLogger(PersonRepository.class);

    public PersonAuthView findPersonByUsername(String username) {
        try {
            Connection connection = DataSourceConfig.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT username, password FROM hospital.user WHERE username = ?");
            preparedStatement.setString(1, username);

            ResultSet resultSet = preparedStatement.executeQuery();
                if (resultSet.next()) {
                    return mapToPersonAuth(resultSet);
                }
        }
        catch (SQLException e) {
            e.printStackTrace();
            logger.error("Getting person failed operation on the database failed.", e);
        }
        return null;
    }

    private PersonAuthView mapToPersonAuth(ResultSet resultSet) throws SQLException {
        PersonAuthView person = new PersonAuthView();
        person.setUsername(resultSet.getString("username"));
        person.setPassword(resultSet.getString("password"));
        return person;
    }

    public List<PersonView> getPersonsView() {
        try {
            Connection connection = DataSourceConfig.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT patient_id, name, surname, date_of_birth, personal_no FROM hospital.patient");
            ResultSet resultSet = preparedStatement.executeQuery();

            List<PersonView> personViews = new ArrayList<>();
            while (resultSet.next()) {
                personViews.add(mapToPersonView(resultSet));
            }
            return personViews;
        }
        catch (SQLException e) {
            e.printStackTrace();
            logger.error("Getting person failed operation on the database failed.", e);
            return null;
        }
    }

    private PersonView mapToPersonView(ResultSet rs) throws SQLException {
        PersonView personView = new PersonView();
        personView.setId(rs.getLong("patient_id"));
        personView.setName(rs.getString("name"));
        personView.setSurname(rs.getString("surname"));
        personView.setDateOfBirth(rs.getString("date_of_birth"));
        personView.setPersonalNo(rs.getString("personal_no"));
        return personView;
    }

    public PersonDetailView findPersonDetailedView(Long id) {
        try {
            Connection connection = DataSourceConfig.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT p.patient_id, name, surname, date_of_birth, personal_no, " +
                            "insurance_company, city, hospital_name, department, floor, room, diagnose, phone_number, street, house_number, zip_code " +
                            "FROM hospital.patient p " +
                            "JOIN hospital.patient_has_address pa ON pa.patient_id = p.patient_id " +
                            "JOIN hospital.address a ON a.address_id= pa.address_id " +
                            "JOIN hospital.hospital_has_patient q ON q.patient_id = p.patient_id " +
                            "JOIN hospital.hospital h ON h.hospital_id=q.hospital_id " +
                            "JOIN hospital.insurance_company i ON i.insurance_company_id = p.insurance_company_id " +
                            "JOIN hospital.patient_has_diagnose x ON x.patient_id = p.patient_id " +
                            "JOIN hospital.diagnose di ON di.diagnose_id = x.diagnose_id " +
                            "JOIN hospital.patient_in_department y ON y.patient_id = p.patient_id " +
                            "JOIN hospital.department de ON de.department_id = y.department_id " +
                            "JOIN hospital.contact c ON c.contact_id = p.contact_id " +
                            "WHERE p.patient_id = ?");
            preparedStatement.setLong(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return mapToPersonDetailView(resultSet);
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
            logger.error("Detail person view on the database failed.", e);
        }
        return null;
    }

    private PersonDetailView mapToPersonDetailView(ResultSet rs) throws SQLException {
        PersonDetailView personDetailView = new PersonDetailView();
        personDetailView.setId(rs.getLong("patient_id"));
        personDetailView.setName(rs.getString("name"));
        personDetailView.setSurname(rs.getString("surname"));
        personDetailView.setDateOfBirth(rs.getString("date_of_birth"));
        personDetailView.setPersonalNo(rs.getString("personal_no"));
        personDetailView.setInsuranceCompany(rs.getString("insurance_company"));
        personDetailView.setCity(rs.getString("city"));
        personDetailView.setHospitalName(rs.getString("hospital_name"));
        personDetailView.setDepartment(rs.getString("department"));
        personDetailView.setFloor(rs.getLong("floor"));
        personDetailView.setRoom(rs.getLong("room"));
        personDetailView.setDiagnose(rs.getString("diagnose"));

        personDetailView.setPhoneNumber(rs.getString("phone_number"));
        personDetailView.setStreet(rs.getString("street"));
        personDetailView.setHouseNumber(rs.getString("house_number"));
        personDetailView.setZipCode(rs.getLong("zip_code"));

        return personDetailView;
    }

    public void editPerson(PersonEditView personEditView) {
        String insertPersonSQL = "UPDATE hospital.patient p SET name = ?, surname = ? WHERE p.patient_id = ?";
        String insertPersonSQL2 = "UPDATE hospital.address a SET city = ?, street = ?, house_number = ?, zip_code = ? FROM hospital.patient_has_address pa, hospital.patient p " +
                "WHERE pa.patient_id = p.patient_id AND a.address_id = pa.address_id AND p.patient_id = ?";

        try {
            Connection connection = DataSourceConfig.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(insertPersonSQL, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, personEditView.getName());
            preparedStatement.setString(2, personEditView.getSurname());
            preparedStatement.setLong(3, personEditView.getId());

            try {
                connection.setAutoCommit(false);

                try (PreparedStatement ps = connection.prepareStatement(insertPersonSQL2, Statement.RETURN_GENERATED_KEYS)) {
                    ps.setString(1, personEditView.getCity());
                    ps.setString(2, personEditView.getStreet());
                    ps.setString(3, personEditView.getHouseNumber());
                    ps.setLong(4, personEditView.getZipCode());
                    ps.setLong(5, personEditView.getId());
                    ps.executeUpdate();
                }
                catch (SQLException e) {
                    logger.error("Edit fail.", e);
                }

                int affectedRows = preparedStatement.executeUpdate();

                if (affectedRows == 0) {
                    logger.error("Edit fail, no rows affected.");
                }
                connection.commit();
            }
            catch (SQLException e) {
                connection.rollback();
            }
            finally {
                connection.setAutoCommit(true);
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
            logger.error("Editing person failed operation on the database failed.", e);
        }
    }


    public void createPerson(PersonCreateView personCreateView) {
        String insertPersonSQL = "INSERT INTO hospital.user (username, password, name, surname) VALUES (?,?,hospital.pgp_sym_encrypt(?,'postgres'),hospital.pgp_sym_encrypt(?,'postgres'))";
        try {
            Connection connection = DataSourceConfig.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(insertPersonSQL);

            preparedStatement.setString(1, personCreateView.getNickname());
            preparedStatement.setString(2, String.valueOf(personCreateView.getPassword()));
            preparedStatement.setString(3, personCreateView.getName());
            preparedStatement.setString(4, personCreateView.getSurname());

            preparedStatement.executeUpdate();
        }
        catch (SQLException e) {
            e.printStackTrace();
            logger.error("Creating person failed.", e);
        }
    }

    public void deletePerson(PersonEditView personEditView) {
        String insertPersonSQL = "DELETE FROM hospital.patient WHERE patient_id = ?";
        try {
            Connection connection = DataSourceConfig.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(insertPersonSQL, Statement.RETURN_GENERATED_KEYS);

            preparedStatement.setLong(1, personEditView.getId());

            preparedStatement.executeUpdate();
        }
        catch (SQLException e) {
            e.printStackTrace();
            logger.error("Deleting person failed.", e);
        }
    }

    public void createPersonStatement(PersonCreateView personCreateView) {
        String insertPersonSQL = "INSERT INTO hospital.user (username, password, name, surname) VALUES ('" + personCreateView.getNickname() + "', '" + String.valueOf(personCreateView.getPassword()) + "', hospital.pgp_sym_encrypt('" + personCreateView.getName() + "', 'postgres'), hospital.pgp_sym_encrypt('" + personCreateView.getSurname() + "', 'postgres'))";
        try {
            Connection connection = DataSourceConfig.getConnection();
            Statement statement = connection.createStatement();
            statement.executeUpdate(insertPersonSQL);
        }
        catch (SQLException e) {
            e.printStackTrace();
            logger.error("Creating person failed by statement.", e);
        }
    }

    public List<PersonView> getPersonsViewBySurname(String surname) {
        try {
            Connection connection = DataSourceConfig.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT patient_id, name, surname, date_of_birth, personal_no FROM hospital.patient WHERE surname = ?");
            preparedStatement.setString(1, surname);//

            ResultSet resultSet = preparedStatement.executeQuery();

            List<PersonView> personViews = new ArrayList<>();
            while (resultSet.next()) {
                personViews.add(mapToPersonView(resultSet));
            }
            return personViews;
        }
        catch (SQLException e) {
            e.printStackTrace();
            logger.error("Getting persons by surname failed.", e);
            return null;
        }
    }

    public List<PersonView> getPersonsViewBySurnameStatement(String surname) {
        String insertPersonSQL = "SELECT patient_id, name, surname, date_of_birth, personal_no FROM hospital.patient WHERE surname = '" + surname + "'";
        try {
            Connection connection = DataSourceConfig.getConnection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(insertPersonSQL);

            List<PersonView> personViews = new ArrayList<>();
            while (resultSet.next()) {
                personViews.add(mapToPersonView(resultSet));
            }
            return personViews;
        }
        catch (SQLException e) {
            e.printStackTrace();
            logger.error("Getting persons failed by statement.", e);
            return null;
        }
    }
}
