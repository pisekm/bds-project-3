package org.but.feec.hospital.service;

import at.favre.lib.crypto.bcrypt.BCrypt;
import org.but.feec.hospital.api.PersonCreateView;
import org.but.feec.hospital.api.PersonDetailView;
import org.but.feec.hospital.api.PersonEditView;
import org.but.feec.hospital.api.PersonView;
import org.but.feec.hospital.data.PersonRepository;

import java.util.List;

public class PersonService {

    private PersonRepository personRepository;

    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public List<PersonView> getPersonsView() {
        return personRepository.getPersonsView();
    }

    public PersonDetailView getPersonDetailView(Long id) {
        return personRepository.findPersonDetailedView(id);
    }

    public void editPerson(PersonEditView personEditView) {
        personRepository.editPerson(personEditView);
    }

    public void createPerson(PersonCreateView personCreateView) {
        char[] originalPassword = personCreateView.getPassword();
        char[] hashedPassword = hashPassword(originalPassword);
        personCreateView.setPassword(hashedPassword);

        personRepository.createPerson(personCreateView);
    }

    private char[] hashPassword(char[] originalPassword) {
        return BCrypt.withDefaults().hashToChar(12, originalPassword);
    }

    public void deletePerson(PersonEditView personEditView) {
        personRepository.deletePerson(personEditView);
    }

    public void createPersonStatement(PersonCreateView personCreateView) {
        char[] originalPassword = personCreateView.getPassword();
        char[] hashedPassword = hashPassword(originalPassword);
        personCreateView.setPassword(hashedPassword);

        personRepository.createPersonStatement(personCreateView);
    }

    public List<PersonView> getPersonsViewBySurname(String surname) {
        return personRepository.getPersonsViewBySurname(surname);
    }

    public List<PersonView> getPersonsViewBySurnameStatement(String surname) {
        return personRepository.getPersonsViewBySurnameStatement(surname);
    }
}
