package org.but.feec.hospital.service;
import at.favre.lib.crypto.bcrypt.BCrypt;
import org.but.feec.hospital.api.PersonAuthView;
import org.but.feec.hospital.data.PersonRepository;


public class AuthService {

    private PersonRepository personRepository;

    public AuthService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    private PersonAuthView findPersonByUsername(String username) {
        return personRepository.findPersonByUsername(username);
    }

    public boolean authenticate(String username, String password) {
        if (username == null || username.isEmpty() || password == null || password.isEmpty()) {
            return false;
        }
        PersonAuthView personAuthView = findPersonByUsername(username);

        if (personAuthView != null) {
            BCrypt.Result result = BCrypt.verifyer().verify(password.toCharArray(), personAuthView.getPassword());
            return result.verified;
        }
        return false;
    }

}